---
ref: incidencia
lang: es
title: Planificación Estrategica
image: tools-07.gif
layout: tool
intro: ¿Buscas aumentar el impacto de tu organización en problemas públicos con herramientas sencillas y transformadoras? Acá podrás definir objetivos concretos, con indicadores de éxito y una estrategia orientada a la incidencia. ¡Échale un ojo!
---

**Planificación Estratégica** comparte criterios para la caracterización de un problema público y elementos para la generación y priorización de objetivos que contribuyan a su solución. Incluye también herramientas de mapeo de agentes clave, así como de seguimiento y evaluación de procesos.

Esta herramienta está dirigida a organizaciones de la sociedad civil, instituciones públicas y organismos internacionales.

### Etapas

1. Primero se crea un encuadre del tema a incidir y luego un análisis colaborativo sobre la organización y el problema que se busca abordar.
2. Se realiza un taller presencial (4 jornadas de trabajo) - facilitado por FCI - para desarrollar la estrategia de incidencia en política pública, permitiendo a la organización alcanzar sus objetivos de impacto.
3. En caso de que la organización lo solicite, FCI puede brindar accesos y asesoría para hacer uso de la plataforma digital Planificación Estratégica. Esto permitirá sistematizar y dar seguimiento a la implementación de la estrategia de incidencia planificada.

### Experiencias destacadas

Trabajamos con la **Fundación Techo para Chile** para la creación del Área de Incidencia. En esa oportunidad la ONG utilizó nuestra metodología a nivel nacional para hacer análisis y planes regionales de incidencia. (Septiembre 2015).

Hemos desarrollado formaciones en Chile, Indonesia, México, Nicaragua y Puerto Rico durantes los años 2016 y 2017.

Ejemplos de su uso: http://estratega.io/


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
