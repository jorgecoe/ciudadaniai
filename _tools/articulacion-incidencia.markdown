---
ref: incidencia
intro: ¿Te gustaría generar iniciativas sociales para solucionar un problema público? ¿Crees que la colaboración entre agentes es un motor de cambio? Esta herramienta es para ti.
lang: es
layout: tool
title: Articulación para la Incidencia
image: tools-01.gif
description: Entregamos herramienta de planificación y seguimiento. Nuestra metodología ayuda a definir objetivos de incidencia desde la pluralidad, así como a crear redes de colaboración interdisciplinaria.
---


**Articulación Para La Incidencia** es una herramientas donde podrás reunir a diversos agentes claves para crear de manera colectiva una estrategia de incidencia a corto plazo y abordando un problema público en específico.


### ¿De qué manera?
Los contenidos de esta iniciativa se enfocan en seis puntos claves:

1. Caracterización de un problema.
2. Definición colaborativa de un sueño.
3. Definición de objetivos.
4. Análisis de contexto y prospección.
5. Elaboración de un plan de trabajo.
6. Generación de una estrategia de seguimiento.

### Etapas

1. Propuesta: En esta instancia podrás establecer un tema relevante en el que buscaremos incidir.
2. Convocatoria: A través de un análisis del contexto sociopolítico y a los agentes claves vinculados a la temática.
3. Implementación: Desde FCI facilitaremos un taller presencial de dos jornadas (6 horas cada uno) para trabajar de manera colaborativa sobre el problema a incidir. Esta instancia nos permite que en conjunto definamos los objetivos para dirigir el proceso y un plan compartido.
4. Resultado: En esta etapa se define una estrategia de incidencia a corto plazo junto a un plan de trabajo sobre la realización del proyecto cívico.
5. Seguimiento: Una vez que implementemos el proyecto se entrega la metodología y estructura de seguimiento.
6. Etapa final: Acompañaremos a las organizaciones que lideran la implementación de lo planificado y este proceso finaliza con una evaluación y reflexión de las lecciones aprendidas.


### Experiencias destacadas

- Hemos facilitado esta herramienta en 15 países de América Latina, y hoy llevamos esta metodología a más contextos y a diferentes áreas. Trabajamos en violencia de género en Honduras, gestión de aguas en Bolivia, violencia armada en juventudes de Colombia, problema de los desechos en Ecuador y Colombia, entre otras.

Ejemplos de su uso: [labcivico.org/casos](https://labcivico.org/casos.html)


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
