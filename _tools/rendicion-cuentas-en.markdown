---
ref: fiscalizacion
lang: en
title: Accountability
image: tools-08.gif
intro: Web platform and methodology that measures and displays the fulfillment of promises made by authorities.
layout: tool
---
