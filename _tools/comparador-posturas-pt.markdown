---
ref: informacion
lang: pt
title: Comparador de posturas
image: tools-02.gif
intro: Plataforma que mostra de forma amigável a visão de duas entidades sobre um mesmo tema e o nível de acordo ou desacordo existente.
layout: tool

---
