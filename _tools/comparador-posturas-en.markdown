---
ref: informacion
lang: en
title: Stance comparison
image: tools-02.gif
intro: Platform that shows in a friendly way the stances of two institutions or entities on a topic, and how much they agree or disagree.
layout: tool

---
