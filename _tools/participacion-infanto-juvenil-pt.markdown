---
ref: participacion
lang: pt
title: Participacão Infantojuvenil
image: tools-06.gif
intro: Uma ferramenta acompanhada de uma metodologia especialmente desenhada para que crianças e adolescentes criem suas propostas de maneira ordenada e sistematizada..
layout: tool
---
