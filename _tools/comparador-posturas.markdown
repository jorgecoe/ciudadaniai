---
ref: informacion
layout: tool
lang: es
title: Comparador de Posturas
image: tools-02.gif
description: Herramienta digital que visualiza a través de una interfaz amigable y atractiva la postura de dos entidades sobre un mismo tema y el nivel de acuerdo o desacuerdo existente.
intro: ¿Te gustaría transformar debates y polémicas en información entendible y amigable? Esta herramienta te permite traducir de forma rápida y dinámica las posiciones de dos grupos respecto a un asunto o conjunto de temas, indicando sus similitudes y diferencias ¡Revísala!

---

Con una metodología muy sencilla y didáctica, **Comparador de Posturas** muestra la visión de dos entidades sobre un mismo tema. Las usuarios o usuarios de esta herramienta, podrán visualizar de manera inmediata el nivel de acuerdo o desacuerdo que existe en entre ambas partes, facilitando el entendimiento de información.

### Beneficios

1. Transparenta el debate.
2. Fomenta la participación y el empoderamiento.
3. Acerca información compleja a toda la comunidad con metodologías inclusivas.

### Experiencias Destacadas

- Revolución Pingüina: Las manifestaciones estudiantiles en Chile durante el 2012 levantaron una discusión profunda y compleja. Esta herramienta permitió seguir el debate sobre la reforma educacional en el país, comparando la postura del gobierno con diferentes actores sociales (Educación 2020, FECH, UDI, Horizontal, Evópoli, Instituto Libertad y Amplitud).
- Sistema Binominal en Chile: Comparador de Posturas permitió contrastar las diferentes visiones respecto de la reforma al sistema binominal en el país.


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
