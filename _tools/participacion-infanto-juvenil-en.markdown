---
ref: participacion
lang: en
title: Children and Youth Participation
image: tools-06.gif
intro: Website accompanied by a methodology especially designed for children and teenagers to raise their proposals in a clear and sistematized way.
layout: tool
---
