---
ref: incidencia
lang: en
title: Activist Training
image: tools-04.gif
intro: We train young leaders that will change their communities and the world with our public advocacy methodologies.
layout: tool

---
