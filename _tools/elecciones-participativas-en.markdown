---
ref: participacion
lang: en
title: Participatory elections
image: tools-03.gif
intro: Platforms for elections that allows citizens to raise their proposals and push candidates to pledge to execute them.
layout: tool
---
