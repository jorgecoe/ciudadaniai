---
ref: participacion
lang: es
title: Elecciones Participativas
image: tools-03.gif
description: Plataforma que permite a la ciudadanía ,en momentos de elecciones, levantar propuestas y presionar a las candidaturas a comprometerse con las demandas ciudadanas.
intro: ¿Consideras que la democracia de tu país no es suficientemente representativa? ¿Quieres que todos los sectores de la sociedad tengan una voz protagónica e incidente? Si buscas crear elecciones más participativas y transparentes acá tenemos la solución.
layout: tool

---

¿Consideras que la democracia de tu país no es suficientemente representativa? ¿El debate público no refleja las necesidades de la ciudadanía? ¿Quieres que todos los sectores de la sociedad tengan una voz protagónica e incidente?

Esta herramienta será tu mejor amiga, porque podrás promover la participación de la ciudadanía en tiempos de elecciones. En **Elecciones Participativas** puedes reunir las propuestas de los distintos grupos y organizaciones en un solo espacio de participación con el objetivo de generar unión entre las personas que buscan ser escuchadas, junto a los candidatas y candidatos de los procesos que se estén llevando a cabo.

¿Cómo se realiza? Este proyecto cuenta con una plataforma dinámica para presentar informativamente los perfiles de las candidaturas en competencia; test de afinidad para electores con las propuestas y posiciones de cada candidatura; herramientas para comparar las posturas de las diferentes candidaturas; y un espacio para que la ciudadanía entregue sus propuestas, permitiendo captar el interés de las candidatas y candidatos para que puedan comprometerse con ellas de forma pública. Además de la entrega de metodologías para la elaboración de propuestas con guías y consejos para la incidencia ciudadana colaborativa y la articulación social.

**Elecciones Participativas** puede ser adoptado a distintos procesos electorales, a nivel nacional, local e incluso de organizaciones o instituciones específicas, facilitando la participación activa e informada de la ciudadanía y de la comunidad.

### Etapas

Sus etapas son las siguientes:

Duración aproximada: tres meses (según complejidad del desafío)

1. Capacitaciones sobre el uso del sitio y herramientas metodológicas.
2. Adaptación de metodologías y materiales para la generación de propuestas, incidencia ciudadana y formación cívica al contexto específico.
3. Generación de perfiles de candidaturas e instalación de herramienta para generación de propuestas.
4. Generación de alianzas con organizaciones para generación de propuestas.
5. Establecimiento de contacto con candidaturas para asegurar su participación activa y compromisos con propuestas.
6. Lanzamiento de test de afinidad política.

### Experiencias destacadas

Han utilizado esta herramienta

- Elecciones de gobiernos locales (municipios) en Chile (2016)
- Elecciones presidenciales y parlamentarias en Chile (2017)

### Beneficios

1. Participación proactiva y propositiva de la ciudadanía.
2. Trabajo colaborativo con organizaciones de la Sociedad Civil.
3. Incidencia ciudadana directa sobre candidaturas y debate electoral.
4. Transparencia sobre candidaturas, sus antecedentes, posiciones políticas y compromisos con la ciudadana.


### Contáctanos

Si crees que esta herramienta es para ti y deseas llevarla a cabo ¡contáctanos!

Escríbenos a **herramientas@ciudadaniai.org**
