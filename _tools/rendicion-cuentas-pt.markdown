---
ref: fiscalizacion
lang: pt
title: Prestação de contas
image: tools-08.gif
intro: Plataforma web e metodologia que mede e visualiza o cumprimento de promessas de autoridades.
layout: tool
---
