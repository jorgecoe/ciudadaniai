---
ref: informacion
lang: pt
title: Perfis Transparentes
image: tools-07.gif
layout: tool
intro: Ferramenta utilizada em processos eleitorais que permite a cidadania conhecer as diferentes candidatura através de uma bateria de perguntas e respostas determinadas.
---
