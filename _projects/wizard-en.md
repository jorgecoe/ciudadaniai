---
layout: project
lang: en
name: "WIZARD Internacional"

description: "Una estrategia de incidencia para UNICEF."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "WIZARD Internacional"
    year: 2015
    country: "Chile"
    description: "Una estrategia de incidencia para UNICEF."
---
