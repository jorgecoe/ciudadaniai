---
layout: project
lang: en
name: "Why in my garden"

description: "Platform developed in alliance with a TV documentary broadcasted by TVN (Chilena national broadcaster) that tells the story of a community that faces the installation of a productive industry."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.cntv.cl/por-que-en-mi-jardin/cntv/2016-07-05/123008.html"
year: 2011

versions:
  - name: "Why in my garden"
    year: 2011
    country: "Chile"
    description: "Platform developed in alliance with a TV documentary broadcasted by TVN (Chilena national broadcaster) that tells the story of a community that faces the installation of a productive industry."
---
