---
layout: project
lang: en
name: "OGP Chile The Alliance for Open Government"

description: "A multilateral initiative seeking to secure concrete commitments from national and subnational governments to promote open government, empower citizens, fight against corruption and to use the new technologies to strengthen governance."

visible_timeline: true
visible_home: false

image: ""
site: "https://ogp.gob.cl/es/principios/"
year: 2016

versions:
  - name: "OGP Chile The Alliance for Open Government"
    year: 2016
    country: "Chile"
    description: "A multilateral initiative seeking to secure concrete commitments from national and subnational governments to promote open government, empower citizens, fight against corruption and to use the new technologies to strengthen governance."
  - name: "OGP Chile The Alliance for Open Government"
    year: 2017
    country: "Chile"
    description: "A multilateral initiative seeking to secure concrete commitments from national and subnational governments to promote open government, empower citizens, fight against corruption and to use the new technologies to strengthen governance."

---
