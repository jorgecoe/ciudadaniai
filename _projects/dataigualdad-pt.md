---
layout: project
lang: pt
name: "Dataigualdad"
link_to_project: true

description: "Veja com gráficos e dados como o privilégio de poucos alimenta a desigualdade na America Latina e Caribe."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/dataigualdad.png"
hero-image: "/assets/images/hero_projects/dataigualdad.png"
logo: "/assets/images/hero_projects/dataigualdad.png"

site: "http://dataigualdad.com/"
category: Fiscaliza
year: 2019

versions:
  - name: "Dataigualdad"
    year: 2019
    country: "Latinoamérica"
    description: "Veja com gráficos e dados como o privilégio de poucos alimenta a desigualdade na America Latina e Caribe."
  - name: "Dataigualdad"
    year: 2020
    country: "Latinoamérica"
    description: "Veja com gráficos e dados como o privilégio de poucos alimenta a desigualdade na America Latina e Caribe."
---
