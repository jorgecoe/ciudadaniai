---
layout: project
lang: pt
name: "Abre Alcaldías"
link_to_project: true
ref: abrealcaldias

description: "Faça parte da construção coletiva de bairros e comunidades em seu município com nossas ferramentas digitais e metodológicas."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/alcaldias.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://abrealcaldias.org/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "Abre Alcaldías"
    year: 2020
    country: "México"
    description: "Una mezcla de herramientas digitales y metodológicas que acercan el trabajo de municipios a vecinas y vecinos, potenciando la construcción colectiva de barrios y comunidades."

---
