---
layout: project
lang: en
name: "Neighbourhoods in Action"

description: "Citizen platform to report neighbourhood issues."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2014

versions:
  - name: "Neighbourhoods in Action"
    year: 2014
    country: "Chile"
    description: "Citizen platform to report neighbourhood issues."
  - name: "Neighbourhoods in Action"
    year: 2015
    country: "Chile"
    description: "Citizen platform to report neighbourhood issues."
---
