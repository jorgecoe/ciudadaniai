---
layout: project
lang: es
name: "Open Budget Survey 2017 Chile"

description: "Una encuesta que evalúa la transparencia del presupuesto según la cantidad y la puntualidad de la información presupuestaria que los gobiernos ponen a disposición del público."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.internationalbudget.org/open-budget-survey/"
year: 2017

versions:
  - name: "Open Budget Survey 2017 Chile"
    year: 2017
    country: "Chile"
    description: "Una encuesta que evalúa la transparencia del presupuesto según la cantidad y la puntualidad de la información presupuestaria que los gobiernos ponen a disposición del público."

---
