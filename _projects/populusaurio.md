---
layout: project
lang: es
name: "Populusaurio"

description: "Una iniciativa que convocaba a la sociedad, familias, organizaciones, y todos los que se quieran sumar, a plantear sus expectativas y propuestas en torno a un Chile que se está construyendo, en contexto de transformaciones y reformas"


visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Populusaurio"
    year: 2016
    country: "Chile"
    description: "Una iniciativa que convocaba a la sociedad, familias, organizaciones, y todos los que se quieran sumar, a plantear sus expectativas y propuestas en torno a un Chile que se está construyendo, en contexto de transformaciones y reformas"

---
