---
layout: project
lang: pt
name: "Tretaqui"
link_to_project: false

description: "Anonimamente denuncia manifestações de violência e incitação ao ódio durante os processos eleitorais no Brasil e que serão redirecionadas para o Ministério Público Federal."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/tretaqui.png"
hero-image: "/assets/images/hero_projects/hero-tretaqui.jpg"
logo: "/assets/images/hero_projects/tretaqui-logo.png"

site: "http://tretaqui.org/"
category: Fiscalização
year: 2018

versions:
  - name: "Tretaqui"
    year: 2018
    country: "Brasil"
    description: "Plataforma para denunciar anonimamente o discurso de ódio durante os processos eleitorais."

---
