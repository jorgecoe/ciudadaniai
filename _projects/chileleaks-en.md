---
layout: project
lang: en
name: "Chileleaks"
link_to_project: true
ref: chileleaks
description: "Do you have information on bribery cases in Chile? Report safely and anonymously through our platform."

description-timeline: "A secure platform to report anonymously and safely bribery cases in Chile."

image: "/assets/images/proyectos/chileleaks.png"
hero-image: "/assets/images/hero_projects/hero-chileleaks.jpg"

visible_timeline: true
visible_home: true

site: "https://chileleaks.org/index.html"
category: Citizen oversight
year: 2018

versions:
  - name: "Chileleaks"
    year: 2018
    country: "Chile"
    description: "A secure platform to report anonymously and safely bribery cases in Chile."


---
