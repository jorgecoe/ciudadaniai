---
layout: project
lang: en
name: "Intelligent Access Chile"

description: "The first platform in Chile that centralized the public information requests system, through an online process."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Intelligent Access Chile"
    year: 2011
    country: "Chile"
    description: "The first platform in Chile that centralized the public information requests system, through an online process."

---
