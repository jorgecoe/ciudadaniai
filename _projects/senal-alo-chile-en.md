---
layout: project
lang: en
name: "Signal it Chile"

description: "Website with information on the location of all cellular antennas in Chile."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2010

versions:
  - name: "Signal it Chile"
    year: 2010
    country: "Chile"
    description: "Website with information on the location of all cellular antennas in Chile."

---
