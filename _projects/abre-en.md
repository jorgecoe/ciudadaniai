---
layout: project
lang: en
name: "Open"

description: "Be part of the collective construction of neighbourhoods and communities in your municipality with our digital and methodological tools."

description-timeline: "Digital and methodological tools to bring the work of municipalities closer to neighbours, fostering the collective construction of neighbourhoods and communities."

image: "/assets/images/proyectos/abre.png"
hero-image: "/assets/images/hero_projects/hero-abre.jpeg"

site: "http://abre.tumunicipio.org/"
category: Citizen participation
year: 2017

visible_timeline: true
visible_home: false

link_to_project: true

versions:
  - name: "Open Peñalolén"
    year: 2017
    country: "Chile"
    description: "Digital and methodological tools to bring the work of municipalities closer to neighbours, fostering the collective construction of neighbourhoods and communities."


---
