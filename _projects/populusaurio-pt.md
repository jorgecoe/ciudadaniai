---
layout: project
lang: pt
name: "Populusaurio"

description: "Uma iniciativa que convocou a sociedade, as famílias, as organizações e todos aqueles que querem acrescentar, a elevar suas expectativas e propostas em torno de um Chile que está sendo construído, em um contexto de transformações e reformas."


visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Populusaurio"
    year: 2016
    country: "Chile"
    description: "Uma iniciativa que convocou a sociedade, as famílias, as organizações e todos aqueles que querem acrescentar, a elevar suas expectativas e propostas em torno de um Chile que está sendo construído, em um contexto de transformações e reformas."

---
