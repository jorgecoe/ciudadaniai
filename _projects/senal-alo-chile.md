---
layout: project
lang: es
name: "Señal aló Chile"

description: "Sitio web con información sobre la ubicación de todas las antenas de celulares de Chile."

image: ""

visible_timeline: true
visible_home: false

site: ""
category:
year: 2010

versions:
  - name: "Señal aló Chile"
    year: 2010
    country: "Chile"
    description: "Sitio web con información sobre la ubicación de todas las antenas de celulares de Chile."

---
