---
layout: project
lang: en
name: "Transparency Agenda"

description: "Platform to inform citizens on the advancement in the legislative process of a bill or a particular commission."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "Transparency Agenda"
    year: 2015
    country: "Chile"
    description: "Platform to inform citizens on the advancement in the legislative process of a bill or a particular commission."

---
