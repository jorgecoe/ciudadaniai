---
layout: project
lang: es
name: "Mujeres Negras Deciden"
link_to_project: true

description: "Revisa la información sobre la campaña Mujeres Negras Deciden y los datos de la sub-representación de las mujeres negras en la política de Brasil."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/mujeresnegrasdeciden.png"
hero-image: "/assets/images/hero_projects/hero-mujeresnegras.png"
logo: "/assets/images/hero_projects/hero-mujeresnegras.png"

site: "https://mulheresnegrasdecidem.org/"
category:
year: 2018

versions:
  - name: "Mujeres Negras Deciden"
    year: 2018
    country: "Brasil"
    description: "Una plataforma de movilización centrada en datos para aumentar la presencia de las mujeres negras en la política institucional en Brasil."
---
