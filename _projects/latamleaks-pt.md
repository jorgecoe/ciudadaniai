---
layout: project
lang: pt
name: "LatamLeaks"
link_to_project: true
ref: latamleaks

description: "Verifique todas as informações para denunciar corrupção e exigir proteção aos denunciantes."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/latamleaks.png"
hero-image: "/assets/images/hero_projects/herolatamleaks.png"

site: "http://latamleaks.lat/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "LatamLeaks"
    year: 2020
    country: "México"
    description: "Verifique nosso site com todas as informações para denunciar corrupção e exigir proteção aos denunciantes."

---
