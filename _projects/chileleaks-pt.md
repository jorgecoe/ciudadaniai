---
layout: project
lang: pt
name: "Chileleaks"
link_to_project: true
ref: chileleaks
description: "Você tem informações sobre casos de suborno e suborno no Chile? Denuncie com segurança e anonimamente através da nossa plataforma."

description-timeline: "Una plataforma segura para denunciar de forma anônima casos de corrupção e suborno no Chile."

image: "/assets/images/proyectos/chileleaks.png"
hero-image: "/assets/images/hero_projects/hero-chileleaks.jpg"

visible_timeline: true
visible_home: true

site: "https://chileleaks.org/index.html"
category: Fiscalização
year: 2018

versions:
  - name: "Chileleaks"
    year: 2018
    country: "Chile"
    description: "Una plataforma segura para denunciar de forma anônima casos de corrupção e suborno no Chile."


---
