---
layout: project
lang: pt
name: "Abre"

description: "Faça parte da construção coletiva de bairros e comunidades em seu município com nossas ferramentas digitais e metodológicas."

description-timeline: "Uma mistura de ferramentas digitais e metodológicas que aproximam o trabalho dos municípios aos vizinhos e vizinhos, promovendo a construção coletiva de bairros e comunidades."

image: "/assets/images/proyectos/abre.png"
hero-image: "/assets/images/hero_projects/hero-abre.jpeg"

site: "http://abre.tumunicipio.org/"
category: Participação cidadã
year: 2017

visible_timeline: true
visible_home: false

versions:
  - name: "Abre Peñalolén"
    year: 2017
    country: "Chile"
    description: "Uma mistura de ferramentas digitais e metodológicas que aproximam o trabalho dos municípios aos vizinhos e vizinhos, promovendo a construção coletiva de bairros e comunidades."


---
