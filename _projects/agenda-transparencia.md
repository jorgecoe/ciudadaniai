---
layout: project
lang: es
name: "Agenda Transparencia"

description: "Plataforma que busca informar a la ciudadanía el estado de avance de alguna ley  o comisión en particular."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "Agenda Transparencia"
    year: 2015
    country: "Chile"
    description: "Plataforma que busca informar a la ciudadanía el estado de avance de alguna ley  o comisión en particular."
---
