---
layout: project
lang: es
name: "Semáforo del Autoritarismo"
link_to_project: true

description: "Una herramienta que permite identificar liderazgos que presentan un riesgo para la democracia. Revisa el caso de Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforoauto.jpg"
hero-image: "/assets/images/hero_projects/semaforoauto.jpg"

site: "https://ciudadaniai.org/campaigns/semaforoautoritario"
year: 2021

versions:
  - name: "Semáforo del Autoritarismo"
    year: 2021
    country: "Chile"
    description: "Una herramienta que permite identificar liderazgos que presentan un riesgo para la democracia. Revisa el caso de Chile."
---
