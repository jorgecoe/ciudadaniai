---
layout: project
lang: pt
name: "Candideit.org Chile"

description: "Primeiro produto como serviço da FCI"

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2012

versions:
  - name: "Candideit.org Chile"
    year: 2012
    country: "Chile"
    description: "Primeiro produto como serviço da FCI"
---
