---
layout: project
lang: pt
name: "Whatsapp Acusete Chile"

description: "Plataforma digital para fazer denúncias de propaganda eleitoral ilegal."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2017

versions:
  - name: "Whatsapp Acusete Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para fazer denúncias de propaganda eleitoral ilegal."


---
