---
layout: project
lang: es
name: "Globo Ciudadano Chile"

description: "Proyecto para registrar a 20 metros de altura las masivas movilizaciones estudiantiles que ocurrían en el país."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Globo Ciudadano Chile"
    year: 2011
    country: "Chile"
    description: "Proyecto para registrar a 20 metros de altura las masivas movilizaciones estudiantiles que ocurrían en el país."

---
