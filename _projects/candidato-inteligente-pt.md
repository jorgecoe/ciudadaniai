---
layout: project
lang: pt
name: "Candidato Inteligente Chile"

description: "Projeto desenvolvido com inteligência artificial para aproximar a política dos cidadãos."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2017

versions:
  - name: "Candidato Inteligente Chile"
    year: 2017
    country: "Chile"
    description: "Projeto desenvolvido com inteligência artificial para aproximar a política dos cidadãos."
---
