---
layout: project
lang: pt
name: "El Vaso Chile"

visible_timeline: true
visible_home: false

description: "O primeiro blog de cidadania inteligente, onde o conteúdo foi regularmente publicado sobre o nosso trabalho."

image: ""
site: ""
year: 2011

versions:
  - name: "El Vaso Chile"
    year: 2011
    country: "Chile"
    description: "O primeiro blog de cidadania inteligente, onde o conteúdo foi regularmente publicado sobre o nosso trabalho."
---
