---
layout: project
lang: es
name: "Hay Acuerdo"

description: "Plataforma digital de comparación de posturas entre la sociedad civil y el gobierno en relación a una problemática coyuntural."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Hay Acuerdo"
    year: 2011
    country: "Chile"
    description: "Plataforma digital de comparación de posturas entre la sociedad civil y el gobierno en relación a una problemática coyuntural."
  - name: "Hay Acuerdo"
    year: 2014
    country: "Chile"
    description: "Plataforma digital de comparación de posturas entre la sociedad civil y el gobierno en relación a una problemática coyuntural."

---
