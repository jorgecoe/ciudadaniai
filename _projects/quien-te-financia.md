---
layout: project
lang: es
name: "¿Quién te financia?"

description: "Sitio web con información sobre la legislación del financiamiento de la política en Chile."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2014

versions:
  - name: "¿Quién te financia?"
    year: 2014
    country: "Chile"
    description: "Sitio web con información sobre la legislación del financiamiento de la política en Chile."
---
