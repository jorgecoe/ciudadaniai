---
layout: project
lang: en
name: "The Asking Whatsapp Chile"

description: "Digital platform to deliver information to citizens on the presidential and parliamentarians election."

visible_timeline: true
visible_home: false

site: ""
category:
year: 2017

versions:
  - name: "The Asking Whatsapp Chile"
    year: 2017
    country: "Chile"
    description: "Digital platform to deliver information to citizens on the presidential and parliamentarians election."
---
