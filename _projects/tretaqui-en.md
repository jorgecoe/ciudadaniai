---
layout: project
lang: en
name: "Tretaqui"
link_to_project: false

description: "Report anonymously violent manifestations and hate speech during electoral processes in Brazil. The reports will be redirect to the Federal Public Ministry."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/tretaqui.png"
hero-image: "/assets/images/hero_projects/hero-tretaqui.jpg"
logo: "/assets/images/hero_projects/tretaqui-logo.png"

site: "http://tretaqui.org/"
category: Citizen oversight
year: 2018

versions:
  - name: "Tretaqui"
    year: 2018
    country: "Brasil"
    description: "Platform to report anonymously and safely hate speech during electoral processes."

---
