---
layout: project
lang: es
name: "Desarrollando América Latina"

description: "Proyecto para la promoción de innovación y emprendimiento social mediante el poder de la tecnología, los datos abiertos y la colaboración entre actores sociales. "

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Desarrollando América Latina"
    year: 2011
    country: "Chile"
    description: "Proyecto para la promoción de innovación y emprendimiento social mediante el poder de la tecnología, los datos abiertos y la colaboración entre actores sociales"
  - name: "Desarrollando América Latina"
    year: 2012
    country: "Chile"
    description: "Proyecto para la promoción de innovación y emprendimiento social mediante el poder de la tecnología, los datos abiertos y la colaboración entre actores sociales."
  - name: "Desarrollando América Latina"
    year: 2014   
    country: "Chile"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de  líderes emergentes en LATAM."
  - name: "Desarrollando América Latina LATAM"
    year: 2015   
    country: "Chile"
    description: "Proyecto de transferencia de herramientas de facilitación a activistas y formación de  líderes emergentes en LATAM."
---
