---
layout: project
lang: pt
name: "Nossa Eleição Guatemala"

description: "Vota Inteligente eleições presidenciais Guatemala."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Nossa Eleição Guatemala"
    year: 2019
    country: "Guatemala"
    description: "Vota Inteligente eleições presidenciais Guatemala."

---
