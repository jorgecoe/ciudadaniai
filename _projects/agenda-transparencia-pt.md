---
layout: project
lang: pt
name: "Agenda Transparente"

description: "Plataforma que procura informar os cidadãos sobre o status do progresso de qualquer lei ou comissão específica."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "Agenda Transparente"
    year: 2015
    country: "Chile"
    description: "Plataforma que procura informar os cidadãos sobre o status do progresso de qualquer lei ou comissão específica."

---
