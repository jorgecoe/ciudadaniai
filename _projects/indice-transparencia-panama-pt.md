---
layout: project
lang: pt
name: "Índice Transparência Panamá"

description: "Guía de políticos de Panamá."

visible_timeline: true
visible_home: false

image: ""
site: "https://espaciocivico.org"
year: 2018

versions:
  - name: "Índice Transparência Panamá"
    year: 2018
    country: "Panamá"
    description: "Guía de políticos de Panamá."

---
