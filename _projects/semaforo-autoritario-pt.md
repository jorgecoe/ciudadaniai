---
layout: project
lang: pt
name: "Semáforo do Autoritarismo"
link_to_project: true

description: "Uma ferramenta que permite identificar lideranças que apresentam risco para a democracia. Reveja o caso do Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforoauto.jpg"
hero-image: "/assets/images/hero_projects/semaforoauto.jpg"

site: "https://ciudadaniai.org/campaigns/semaforoautoritario"
year: 2021

versions:
  - name: "Semáforo do Autoritarismo"
    year: 2021
    country: "Chile"
    description: "Uma ferramenta que permite identificar lideranças que apresentam risco para a democracia. Reveja o caso do Chile."
---
