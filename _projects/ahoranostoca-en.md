---
layout: project
lang: en
name: "Now it's Our Turn to Participate"
link_to_project: true
ref: ahoranostoca
description: "Check how we promote citizen participation at national level during political processes in Chile after the social outbreak of October 2019."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/antp.png"
hero-image: "/assets/images/hero_projects/hero-ahoranostoca.png"

site: "https://ahoranostocaparticipar.cl/"
year: 2020

versions:
  - name: "Now it's Our Turn to Participate"
    year: 2020
    country: "Chile"
    description: "Check how we promote citizen participation at national level during political processes in Chile after the social outbreak of October 2019."
---
