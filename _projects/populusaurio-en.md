---
layout: project
lang: en
name: "Populusaurio"

description: "An initiative to convene society, families, organizations, and everyone wanting to join, to share their expectations and proposals around a Chile that is been built, in the context of transformations and reforms."


visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Populusaurio"
    year: 2016
    country: "Chile"
    description: "An initiative to convene society, families, organizations, and everyone wanting to join, to share their expectations and proposals around a Chile that is been built, in the context of transformations and reforms."

---
