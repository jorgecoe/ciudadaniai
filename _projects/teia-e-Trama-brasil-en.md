---
layout: project
lang: en
name: "Web and Plot Brazil"

description: "Project that aimed to expose the role played by Brazilian companies in the institutional political crisis in Brazil, in the context of the Lava Jato operation."

visible_timeline: true
visible_home: false

image: ""
site: "https://ateiaeatrama.org/"
year: 2017

versions:
  - name: "Web and Plot Brazil"
    year: 2017
    country: "Brasil"
    description: "Project that aimed to expose the role played by Brazilian companies in the institutional political crisis in Brazil, in the context of the Lava Jato operation."

---
