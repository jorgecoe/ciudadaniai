---
layout: project
lang: en
name: "NEBE Bolivia"

description: "Project that studied the impacts of nationalization on conflicts and cooperation around extractive activities in Bolivia, Ecuador and Perú."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "NEBE Bolivia"
    year: 2015
    country: "Bolivia"
    description: "Project that studied the impacts of nationalization on conflicts and cooperation around extractive activities in Bolivia, Ecuador and Perú."

---
