---
layout: project
lang: es
name: "Chile Marca Preferencia"
link_to_project: true
ref: chilemarcapreferencia

description: "Revisa y compara las posturas de los principales actores políticos sobre más de 20 temas clave para Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/chilemarca-thumb.png"
hero-image: "/assets/images/hero_projects/hero aa.png"

site: "https://chilemarcapreferencia.cl/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "Chile Marca Preferencia "
    year: 2020
    country: "Chile"
    description: "Un sitio que pone a disposición de la ciudadanía información comparable y transparente sobre las posiciones de distintos actores, como partidos políticos y organizaciones sociales, en distintas áreas temáticas clave para Chile."

---
