---
layout: project
lang: en
name: "Citizen Globe Chile"

description: "Project to register, from 20 meters high, massive student mobilizations happening in the country."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "Citizen Globe Chile"
    year: 2011
    country: "Chile"
    description: "Project to register, from 20 meters high, massive student mobilizations happening in the country."

---
