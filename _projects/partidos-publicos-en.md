---
layout: project
lang: en
name: "Public Parties"

description: "Access and monitor in an interactive and friendly way public information on political parties active in the Transparency Portal of Chile."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/partidos-publicos.png"
hero-image: "/assets/images/hero_projects/hero-partidospublicos.jpg"

site: "https://partidospublicos.cl/"
category: Citizen oversight
year: 2016

versions:
  - name: "Public Parties"
    year: 2016
    country: "Chile"
    description: "A platform that looks to provide access to and citizen supervision on information published by Chilean political parties as active transparency, through visualizations, filters and comparisons."

---
