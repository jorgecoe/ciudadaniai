---
layout: project
lang: pt
name: "LabCívico"

description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

visible_timeline: true
visible_home: false

image: ""
site: "https://labcivico.org/"
year: 2015

versions:
  - name: "LabCívico Chile"
    year: 2015
    country: "Chile"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Chile"
    year: 2016
    country: "Chile"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Costa Rica"
    year: 2016
    country: "Costa Rica"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Guatemala"
    year: 2016
    country: "Guatemala"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Honduras"
    year: 2016
    country: "Honduras"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico sobre saúde pública, Rio de Janeiro, Brasil"
    year: 2017
    country: "Brasil"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Belém, Brasil "
    year: 2017
    country: "Brasil"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico México"
    year: 2017
    country: "México"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."
  - name: "LabCívico Panamá"
    year: 2017
    country: "Panamá"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

  - name: "LabCívico Alocação de gastos públicos em Porto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

  - name: "LabCívico Igualdade de Gênero em el Mar México"
    year: 2018
    country: "México"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

  - name: "LabCívico Inversão no setor social em Porto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

  - name: "Labcívico movimento feminista Chile"
    year: 2018
    country: "Chile"
    description: "Metodologia criada pela Cidadania Inteligente para desenvolver estratégias colaborativas de defesa de interesses que permitam que organizações e atores da sociedade civil encontrem soluções para um problema comum."

---
