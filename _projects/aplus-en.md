---
layout: project
lang: en
name: "&lt;A+&gt; Alliance for Inclusive Algorithms"
link_to_project: true
ref: aplus
description: "Be part of the global alliance for Inclusive Algorithms to combat the biases reproduced by Artificial Intelligence."

description-timeline: "Global alliance for Inclusive Algorithms to combat the biases reproduced by Artificial Intelligence."

image: "/assets/images/proyectos/aplus.png"
hero-image: "/assets/images/hero_projects/hero-aplus.jpeg"

site: "http://aplusalliance.org/"
category: Gender and technology
year: 2019

visible_timeline: true
visible_home: true

versions:
  - name: "<A+> Alliance for Inclusive Algorithms"
    year: 2019
    country: "Global"
    description: "Global alliance for Inclusive Algorithms to combat the biases reproduced by Artificial Intelligence."
    versions:
  - name: "<A+> Alliance for Inclusive Algorithms"
    year: 2020
    country: "Global"
    description: "Global alliance for Inclusive Algorithms to combat the biases reproduced by Artificial Intelligence."

---
