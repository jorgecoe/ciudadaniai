---
layout: project
lang: pt
name: "Cabildeas Chile"

description: "Projeto que incentivou a participação dos cidadãos no processo constituinte da nova Constituição do Chile."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Cabildeas Chile"
    year: 2016
    country: "Chile"
    description: "Projeto que incentivou a participação dos cidadãos no processo constituinte da nova Constituição do Chile."


---
