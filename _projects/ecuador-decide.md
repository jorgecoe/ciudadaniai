---
layout: project
lang: es
name: "Ecuador Decide"

description: "Vota Inteligente elecciones municipales Ecuador."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Ecuador Decide"
    year: 2019
    country: "Ecuador"
    description: "Vota Inteligente elecciones municipales Ecuador."

---
