---
layout: project
lang: en
name: "Open Budget Survey 2017 Chile"

description: "A survey that assess the transparency in public budget according to the amount and specificity of budget information that governments make public."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.internationalbudget.org/open-budget-survey/"
year: 2017

versions:
  - name: "Open Budget Survey 2017 Chile"
    year: 2017
    country: "Chile"
    description: "A survey that assess the transparency in public budget according to the amount and specificity of budget information that governments make public."

---
