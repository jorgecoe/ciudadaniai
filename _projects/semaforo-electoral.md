---
layout: project
lang: es
name: "Semáforo Electoral"
link_to_project: true

description: "Revisa qué se puede hacer durante la propaganda electoral y cómo denunciar cuando no se cumple la ley."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforo1.png"
hero-image: "/assets/images/hero_projects/semaforoelectoral.png"

site: "http://semaforoelectoral.cl/"
year: 2016

versions:
  - name: "Semáforo Electoral"
    year: 2016
    country: "Chile"
    description: "Un plataforma que aclara qué se puede o no hacer en diferentes periodos de campañas electorales."
  - name: "Semáforo Electoral"
    year: 2017
    country: "Chile"
    description: "Un plataforma que aclara qué se puede o no hacer en diferentes periodos de campañas electorales."
  - name: "Semáforo Electoral"
    year: 2019
    country: "Chile"
    description: "Un plataforma que aclara qué se puede o no hacer en diferentes periodos de campañas electorales."
---
