---
layout: project
lang: pt
name: "Vota Inteligente"
link_to_project: true

description: "Criar e apoiar propostas, durante os processos eleitorais, que serão enviadas aos candidatos para inclusão em seus programas de governo."

description-timeline: "Primeiro projeto de cidadania inteligente. Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/vota.png"
hero-image: "/assets/images/hero_projects/hero-vota.jpg"

site: "https://votainteligente.cl/"
category: Denuncia Cohecho
year: 2009
status: Activo

versions:
  - name: "Vota Inteligente Chile"
    year: 2009
    country: "Chile"
    description: "Primeiro projeto de cidadania inteligente. Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Chile"
    year: 2011
    country: "Chile"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Chile"
    year: 2012
    country: "Chile"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Chile"
    year: 2013
    country: "Chile"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Chile"
    year: 2014
    country: "Chile"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Mexico"
    year: 2016
    country: "México"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."
  - name: "Vota Inteligente Municipal Chile"
    year: 2016
    country: "Chile"
    description: "Ferramenta digital para informar a cidadania sobre as candidaturas presidenciais, colocando a tecnologia à disposição da política."

---
