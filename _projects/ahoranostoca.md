---
layout: project
lang: es
name: "Ahora Nos Toca Participar"
link_to_project: true
ref: ahoranostoca

description: "Revisa como promovemos la participación ciudadana a nivel nacional durante los procesos políticos en Chile luego del estallido social de octubre de 2019."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/antp.png"
hero-image: "/assets/images/hero_projects/hero-ahoranostoca.png"

site: "https://ahoranostocaparticipar.cl/"
year: 2020

versions:
  - name: "Ahora Nos Toca Participar"
    year: 2020
    country: "Chile"
    description: "Revisa como promovemos la participación ciudadana a nivel nacional durante los procesos políticos en Chile luego del estallido social de octubre de 2019."
---
