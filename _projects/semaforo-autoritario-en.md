---
layout: project
lang: en
name: "Traffic light of Authoritarianism"
link_to_project: true

description: "A tool that allows identifying leaderships that present a risk for democracy. Check the case of Chile."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforoauto.jpg"
hero-image: "/assets/images/hero_projects/semaforoauto.jpg"

site: "https://ciudadaniai.org/campaigns/semaforoautoritario"
year: 2021

versions:
  - name: "Traffic light of Authoritarianism"
    year: 2021
    country: "Chile"
    description: "A tool that allows identifying leaderships that present a risk for democracy. Check the case of Chile."
---
