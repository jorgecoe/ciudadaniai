---
layout: project
lang: es
name: "Monitoreo Legislativo Chile"

description: "Plataforma digital para acceder a información de votación y presentación de proyectos de legisladores electxs."

image: ""

visible_timeline: true
visible_home: false

site: ""
category:
year: 2010

versions:
  - name: "Monitoreo Legislativo Chile"
    year: 2010
    country: "Chile"
    description: "Plataforma digital para acceder a información de votación y presentación de proyectos de legisladores electxs."

---
