---
layout: project
lang: pt
name: "&lt;A+&gt; Aliança por Algoritmos Inclusivos"
link_to_project: true
ref: aplus
description: "Participe da aliança global de algoritmos inclusivos para combater os preconceitos reproduzidos pela Inteligência Artificial."

description-timeline: "Aliança global de algoritmos inclusivos para combater os preconceitos reproduzidos pela Inteligência Artificial."

image: "/assets/images/proyectos/aplus.png"
hero-image: "/assets/images/hero_projects/hero-aplus.jpeg"

site: "http://aplusalliance.org/"
category: Gênero e Tecnologia
year: 2019

visible_timeline: true
visible_home: true

versions:
  - name: "<A+> Aliança por Algoritmos Inclusivos"
    year: 2019
    country: "Global"
    description: "Aliança global de algoritmos inclusivos para combater os preconceitos reproduzidos pela Inteligência Artificial."
  - name: "<A+> Aliança por Algoritmos Inclusivos"
    year: 2020
    country: "Global"
    description: "Aliança global de algoritmos inclusivos para combater os preconceitos reproduzidos pela Inteligência Artificial."

---
