---
layout: project
lang: en
name: "Latin American Legislative Transparency Network"

description: "Network that actively promotes transparency, access to information and responsibility in the congresses of Latin America. Ciudadanía Inteligente coordinated the network."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.transparencialegislativa.org/"
year: 2016

versions:
  - name: "Latin American Legislative Transparency Network"
    year: 2016
    country: "Chile"
    description: "Network that actively promotes transparency, access to information and responsibility in the congresses of Latin America. Ciudadanía Inteligente coordinated the network. "
  - name: "Latin American Legislative Transparency Network"
    year: 2017
    country: "Chile"
    description: "Network that actively promotes transparency, access to information and responsibility in the congresses of Latin America. Ciudadanía Inteligente coordinated the network. "
---
