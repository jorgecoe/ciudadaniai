---
layout: project
lang: es
name: "Ley del Lobby"

description: "Antes de que Chile regulara la Ley de Lobby creamos una plataforma para transparentar las actividades de lobby que se ejercen sobre autoridades y funcionarios."

visible_timeline: true
visible_home: false

image: ""
site: "http://leydelobby.cl/preguntas-frecuentes.html"
year: 2014

versions:
  - name: "Ley del Lobby"
    year: 2014
    country: "Brasil"
    description: "Antes de que Chile regulara la Ley de Lobby creamos una plataforma para transparentar las actividades de lobby que se ejercen sobre autoridades y funcionarios."

---
