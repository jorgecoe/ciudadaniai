---
layout: project
lang: es
name: "Vigilantes por la infancia Chile"
link_to_project: true

description: "Revisa el nivel de cumplimiento del Gobierno de Chile sobre los compromisos adquiridos en el Acuerdo Nacional por la Infancia."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/vigilantes.png"
hero-image: "/assets/images/hero_projects/hero-vigilantes.png"
logo: "/assets/images/hero_projects/hero-vigilantes.png"

site: "https://vigilantesporlainfancia.cl/"
category: Fiscaliza
year: 2018

versions:
  - name: "Vigilantes por la infancia Chile"
    year: 2018
    country: "Chile"
    description: "Revisa el nivel de cumplimiento del Gobierno de Chile sobre los compromisos adquiridos en el Acuerdo Nacional por la Infancia."

---
