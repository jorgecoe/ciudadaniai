---
layout: project
lang: pt
name: "Estratégia Chile"

description: "Inovação Social Aysén Chile."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2015

versions:
  - name: "Estratégia Chile"
    year: 2015
    country: "Chile"
    description: "Inovação Social Aysén Chile."
  - name: "Estratégia México"
    year: 2016
    country: "México"
    description: "Inovação Social México."
  - name: "Estratégia Porto Rico"
    year: 2016
    country: "México"
    description: "Inovação Social Puerto Rico."
  - name: "Estratégia Porto Rico"
    year: 2017
    country: "Porto Rico"
    description: "Inovação Social Puerto Rico."
  - name: "Estratégia Nicarágua"
    year: 2017
    country: "Nicaragua"
    description: "Inovação Social Nicaragua."
  - name: "Estratégia Indonesia"
    year: 2017
    country: "Indonesia"
    description: "Inovação Social Indonesia."
---
