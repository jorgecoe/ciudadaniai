---
layout: project
lang: es
name: "No Más Cohecho"
link_to_project: true

description: "Ayúdanos a detener la corrupción, luchar por mayor transparencia, y por asegurar que nuestra democracia sirva al interés general, y no al de grupos privilegiados."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/nomascohecho.png"
hero-image: "/assets/images/hero_projects/hero-nomascohecho.jpg"

site: "https://nomascohecho.cl/"
category: Participación Ciudadana
year: 2018

countries:
  - country: Chile
    latitude: 23.34
    longitude: 34.56
    description: "Ayúdanos a dar fin a la corrupción, buscamos sancionar con el máximo rigor que permite la ley a los responsables de defraudar la fe pública."
    color: "#565678"

versions:
  - name: "No más cohecho Chile"
    year: 2015
    country: "Chile"
    description: "Plataforma digital para la generación de propuestas ciudadanas contra el cohecho y de entrega de información de las querellas presentadas por Ciudadanía Inteligente contra personajes políticos chilenos involucrados en casos de cohecho y financiamiento ilegal de la política."


---
