---
layout: project
lang: pt
name: "Semáforo eleitoral"
link_to_project: true

description: "Revise o que se pode fazer durante a propaganda eleitoral e como denunciar quando não se cumpre a lei."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/semaforo1.png"
hero-image: "/assets/images/hero_projects/semaforoelectoral.png"

site: ""
year: 2016

versions:
  - name: "Semáforo eleitoral"
    year: 2016
    country: "Chile"
    description: "Uma plataforma que esclarece o que pode ou não ser feito em diferentes períodos de campanhas eleitorais."
  - name: "Semáforo eleitoral"
    year: 2017
    country: "Chile"
    description: "Uma plataforma que esclarece o que pode ou não ser feito em diferentes períodos de campanhas eleitorais."
  - name: "Semáforo eleitoral"
    year: 2019
    country: "Chile"
    description: "Uma plataforma que esclarece o que pode ou não ser feito em diferentes períodos de campanhas eleitorais."
---
