---
layout: project
lang: es
name: "Levanta la Mano"
link_to_project: true

description: "Fomenta la participación y la conciencia cívica de los niños, niñas y adolescentes para que puedan crear propuestas a problemáticas percibidas en su escuela o contexto local."

description-timeline: "Plataforma digital para fomentar participación y conciencia cívica de niños, niñas y adolescentes en la vida política de su país. "

image: "/assets/images/proyectos/LLM.png"
hero-image: "/assets/images/hero_projects/hero-LLM.jpg"

visible_timeline: true
visible_home: false

site: "http://levantalamano.org/"
category: Participación Infanto Juvenil
year: 2018

versions:
  - name: "Levanta la Mano Chile"
    year: 2017
    country: "Chile"
    description: "Plataforma digital para fomentar participación y conciencia cívica de niños, niñas y adolescentes en la vida política de su país."

  - name: "Levanta la mano por el futuro Hijuelas Chile"
    year: 2019
    country: "Chile"
    description: "Plataforma digital para fomentar participación y conciencia cívica de niños, niñas y adolescentes en la vida política de su país."

---
