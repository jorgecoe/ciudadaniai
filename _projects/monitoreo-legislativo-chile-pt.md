---
layout: project
lang: pt
name: "Monitoramento Legislativo Chile"

description: "Plataforma digital para acessar informações de votação e apresentação de projetos de legisladores eletivos."

image: ""

visible_timeline: true
visible_home: false

site: ""
category:
year: 2010

versions:
  - name: "Monitoramento Legislativo Chile"
    year: 2010
    country: "Chile"
    description: "Plataforma digital para acessar informações de votação e apresentação de projetos de legisladores eletivos."

---
