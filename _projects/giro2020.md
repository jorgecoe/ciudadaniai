---
layout: project
lang: es
name: "GIRO2020"
link_to_project: true
ref: giro2020

description: "Revisa cómo impulsamos la participación ciudadana y la movilización social en las elecciones municipales del estado de Río de Janeiro."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/giro.png"

site: "https://giro2020.org/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "GIRO2020"
    year: 2020
    country: "Brasil"
    description: "Una iniciativa que busca aportar,durante todo el proceso de elecciones municipales en el Estado de Río de Janeiro, mayor participación ciudadana y capacitación a candidaturas."

---
