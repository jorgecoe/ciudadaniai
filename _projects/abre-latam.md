---
layout: project
lang: es
name: "Abrelatam"

description: "Proyecto para la construcción y promoción de datos abiertos, políticas y sus usos para mejorar la situación de América Latina."

visible_timeline: true
visible_home: false

image: ""
site: "https://2019.abrelatam.org/"
year: 2015

versions:
  - name: "Abrelatam"
    year: 2015
    country: "Chile"
    description: "Proyecto para la construcción y promoción de datos abiertos, políticas y sus usos para mejorar la situación de América Latina."

---
