---
layout: project
lang: en
name: "The Constitution is Ours"
link_to_project: true
ref: alcen

description: "Let's promote a Constitution written by citizens. Join us with your proposal and we will send it to the Convention!"

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/lcen.png"
hero-image:

site: "https://laconstitucionesnuestra.cl/"
category: Participación Ciudadana
year: 2021

versions:
  - name: "The Constitution is Ours"
    year: 2021
    country: "Chile"
    description: "An open, collective and collaborative platform promoted by Ciudadanía Inteligente, the Global Initiative for Economic Rights, Consti Tu + Yo and the FES, which seeks to make visible and articulate citizen proposals for the new Constitution that will be connected with the daily work of the conventionals."

---
