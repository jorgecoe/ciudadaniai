---
layout: project
lang: en
name: "Me Representa"
link_to_project: true

description: "Know here which candidates are committed with human rights and which better represent your political stances during electoral periods."

description-timeline: "A platform that connected citizens and candidacies committed with human rights."

image: "/assets/images/proyectos/merepresenta.png"
hero-image: "/assets/images/hero_projects/hero-merepresenta.jpg"

visible_timeline: true
visible_home: false

category: Effective Information
year: 2018

versions:
  - name: "Me Representa Brazil"
    year: 2018
    country: "Brazil"
    description: "A platform that connected citizens and candidacies committed with human rights."
---
