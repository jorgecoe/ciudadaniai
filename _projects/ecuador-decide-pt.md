---
layout: project
lang: pt
name: "Ecuador Decide"

description: "Vota Inteligente eleições municipais Equador."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2019

versions:
  - name: "Ecuador Decide"
    year: 2019
    country: "Ecuador"
    description: "Vota Inteligente eleições municipais Equador "

---
