---
layout: project
lang: es
name: "Cabildeas Chile"

description: "Proyecto que fomentaba la participación de la ciudadanía en el proceso constituyente para la nueva Constitución de Chile."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2016

versions:
  - name: "Cabildeas Chile"
    year: 2016
    country: "Chile"
    description: "Proyecto que fomentaba la participación de la ciudadanía en el proceso constituyente para la nueva Constitución de Chile."

---
