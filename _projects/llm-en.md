---
layout: project
lang: en
name: "Raise Your Hand"
link_to_project: true

description: "Platform promoting participation and civic education among children and teenagers promoting the creation of proposals to address issues faced by their school or local context."

description-timeline: "Digital platform to promote the participation and civic conscioiusness of children and teenagers."

image: "/assets/images/proyectos/LLM.png"
hero-image: "/assets/images/hero_projects/hero-LLM.jpg"

visible_timeline: true
visible_home: false

site: "http://levantalamano.org/"
category: Children and Youth Participation
year: 2018

versions:
  - name: "Raise Your Hand Chile"
    year: 2017
    country: "Chile"
    description: "Digital platform to promote the participation and civic conscioiusness of children and teenagers."

  - name: "Raise Your Hand For the Future Hijuelas, Chile"
    year: 2019
    country: "Chile"
    description: "Digital platform to promote the participation and civic conscioiusness of children and teenagers."

---
