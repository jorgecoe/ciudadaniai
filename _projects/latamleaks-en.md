---
layout: project
lang: en
name: "LatamLeaks"
link_to_project: true
ref: latamleaks

description: "Check our site with all the information to report corruption and demand protection for whistleblowers."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/latamleaks.png"
hero-image: "/assets/images/hero_projects/herolatamleaks.png"

site: "http://latamleaks.lat/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "LatamLeaks"
    year: 2020
    country: "México"
    description: "Our site with all the information to report corruption and demand protection for whistleblowers."

---
