---
layout: project
lang: es
name: "LatamLeaks"
link_to_project: true
ref: latamleaks

description: "Revisa toda la información para denunciar la corrupción y exigir protección para las personas alertadoras."

visible_timeline: true
visible_home: true

image: "/assets/images/proyectos/latamleaks.png"
hero-image: "/assets/images/hero_projects/herolatamleaks.png"

site: "http://latamleaks.lat/"
category: Participación Ciudadana
year: 2020

versions:
  - name: "LatamLeaks"
    year: 2020
    country: "México"
    description: "Plataforma que reúne toda la información para denunciar la corrupción y exigir protección para las personas alertadoras."

---
