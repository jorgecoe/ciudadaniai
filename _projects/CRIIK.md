---
layout: project
lang: es
name: "CRIIK Chile"

description: "Plataforma digital de datos abiertos para permitir a la ciudadanía acceder y compartir  información pública de sus gobiernos de manera online."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2011

versions:
  - name: "CRIIK Chile"
    year: 2011
    country: "Chile"
    description: "Plataforma digital de datos abiertos para permitir a la ciudadanía acceder y compartir  información pública de sus gobiernos de manera online."
  - name: "CRIIK Chile"
    year: 2016
    country: "Chile"
    description: "Plataforma digital de datos abiertos para permitir a la ciudadanía acceder y compartir  información pública de sus gobiernos de manera online."

---
