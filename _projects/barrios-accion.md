---
layout: project
lang: es
name: "Barrios en Acción"

description: "Plataforma ciudadana para reportar problemas de barrio."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2014

versions:
  - name: "Barrios en Acción"
    year: 2014
    country: "Chile"
    description: "Plataforma ciudadana para reportar problemas de barrio. "
  - name: "Barrios en Acción"
    year: 2015
    country: "Chile"
    description: "Plataforma ciudadana para reportar problemas de barrio. "

---
