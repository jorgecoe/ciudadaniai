---
layout: project
lang: en
name: "LabCívico Chile"

description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

visible_timeline: true
visible_home: false

image: ""
site: "https://labcivico.org/"
year: 2015

versions:
  - name: "LabCívico Chile"
    year: 2015
    country: "Chile"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Chile"
    year: 2016
    country: "Chile"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Costa Rica"
    year: 2016
    country: "Costa Rica"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Guatemala"
    year: 2016
    country: "Guatemala"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Honduras"
    year: 2016
    country: "Honduras"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico on public health, Rio de Janeiro, Brazil"
    year: 2017
    country: "Brasil"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Belén, Brazil"
    year: 2017
    country: "Brasil"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Mexico"
    year: 2017
    country: "México"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."
  - name: "LabCívico Panama"
    year: 2017
    country: "Panamá"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

  - name: "LabCívico Public Spending Allocation Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

  - name: "LabCívico Gender Equality in the Sea México"
    year: 2018
    country: "México"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

  - name: "LabCívico Social Sector Investment Puerto Rico"
    year: 2018
    country: "Puerto Rico"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

  - name: "LabCívico Feminist Movement Chile"
    year: 2018
    country: "Chile"
    description: "Methodology created by Ciudadanía Inteligente to develop advocacy collaborative strategies that allow organizations and civil society actors to find solutions to a common problem."

---
