---
layout: project
lang: es
name: "Del Dicho al Hecho"
link_to_project: true

description: "Infórmate con nuestro estudio cuánto cumplen los gobiernos sus promesas legislativas en sus programas de gobierno y discursos de rendición de cuentas."

image: "/assets/images/proyectos/ddah.png"
hero-image: "/assets/images/hero_projects/hero-DDAH.jpg"

visible_timeline: true
visible_home: true

site: "https://deldichoalhecho.cl/"
category: Fiscaliza
year: 2010-2019

versions:
  - name: "Del Dicho al Hecho"
    year: 2011
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho"
    year: 2012
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho"
    year: 2013
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho"
    year: 2014
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho"
    year: 2015
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Cuenta Pública Presidencial Uruguay"
    year: 2016
    country: "Uruguay"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Cuenta Pública Presidencial Chile"
    year: 2016
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Programa de Gobierno Chile"
    year: 2016
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Cuenta Pública Presidencial Uruguay"
    year: 2017
    country: "Uruguay"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Cuenta Pública Presidencial Chile"
    year: 2017
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Programa de Gobierno Chile"
    year: 2017
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Dito e Feito Brasil"
    year: 2017
    country: "Brasil"
    description: "Del Dicho Al Hecho cuenta pública alcalde Río de Janeiro"
  - name: "Del Dicho Al Hecho cuenta pública presidencial Uruguay"
    year: 2018
    country: "Uruguay"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Cuenta Pública Presidencial Chile"
    year: 2019
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Programa de Gobierno Chile"
    year: 2019
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
  - name: "Del Dicho al Hecho Programa de Gobierno Chile"
    year: 2020
    country: "Chile"
    description: "Un estudio que mide el cumplimiento de las promesas legislativas establecidas por el gobierno en su programa presidencial y discurso de rendición de cuentas."
---
