---
layout: project
lang: pt
name: "Por qué en mi jardin"

description: "Plataforma em parceria com série documental do canal de televisão TVN que conta a história de uma comunidade que enfrenta a instalação iminente de uma indústria produtiva em suas comunidades."

visible_timeline: true
visible_home: false

image: ""
site: "https://www.cntv.cl/por-que-en-mi-jardin/cntv/2016-07-05/123008.html"
year: 2011

versions:
  - name: "Por qué en mi jardin"
    year: 2011
    country: "Chile"
    description: "Plataforma em parceria com série documental do canal de televisão TVN que conta a história de uma comunidade que enfrenta a instalação iminente de uma indústria produtiva em suas comunidades."
---
