---
layout: project
lang: pt
name: "Bairros em Ação Chile"

description: "Plataforma ciudadana para reportar problemas de barrio."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2014

versions:
  - name: "Bairros em Ação Chile"
    year: 2014
    country: "Chile"
    description: "Plataforma ciudadana para reportar problemas de barrio."
  - name: "Bairros em Ação Chile"
    year: 2015
    country: "Chile"
    description: "Plataforma ciudadana para reportar problemas de barrio."

---
