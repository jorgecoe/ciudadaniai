---
layout: project
lang: es
name: "Partidos Públicos"

description: "Accede y fiscaliza de forma interactiva y amigable la información pública de los partidos políticos activos en el Portal de Transparencia de Chile."

visible_timeline: true
visible_home: false

image: "/assets/images/proyectos/partidos-publicos.png"
hero-image: "/assets/images/hero_projects/hero-partidospublicos.jpg"

site: "https://partidospublicos.cl/"
category: Fiscaliza
year: 2016

countries:
  - country: Chile
    latitude: 23.34
    longitude: 34.56
    description: "Accede y fiscaliza de forma interactiva y amigable la información de los partidos activos en el Portal de Transparencia."
    color: "#565678"

versions:
  - name: "Partidos Públicos"
    year: 2016
    country: "Chile"
    description: "Una plataforma que busca facilitar el acceso y la fiscalización ciudadana a la información que publican los partidos políticos chilenos en transparencia activa, a través de visualizaciones, filtros y comparativas amigables."

---
