---
layout: project
lang: pt
name: "Hacking Cívico Brasil"

description: "Um curso sobre geração e treinamento de projetos sobre os desafios das democracias e geração de soluçõe no Brasil."

visible_timeline: true
visible_home: false

image: ""
site: ""
year: 2017

versions:
  - name: "Hacking Cívico Brasil"
    year: 2017
    country: "Brasil"
    description: "Um curso sobre geração e treinamento de projetos sobre os desafios das democracias e geração de soluçõe no Brasil."

---
