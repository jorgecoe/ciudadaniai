---
layout: campaigns/covid19
version: Informe COVID19
title: '¿Qué tanta información están entregando los gobiernos latinoamericanos sobre COVID-19?'
call: 'Revisa el informe'
summary: En tiempos de crisis, la transparencia es vital. Permite a especialistas y a la ciudadanía tener datos para tomar decisiones informadas y exigir sus derechos.
img: slides/covid.jpg
anchor: informe
update: Datos levantos entre el 7 y 13 de julio
---
