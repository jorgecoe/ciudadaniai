---
layout: post
title: 'Gobiernos locales y su compromiso en el proceso constituyente'
intro: '¿Qué pueden hacer los municipios para impulsar la participación ciudadana? Revisa este post.'
day: 21
month: abril
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Gobiernos locales y su compromiso en el proceso constituyente'
img: 21-4-21.jpg
caption:
---
Junto a la histórica elección de Convencionales, también enfrentaremos las elecciones de Gobernadores Regionales, Alcaldes y Concejales que representan desafíos y oportunidades para la democracia en Chile. Los municipios juegan un rol fundamental en la calidad de vida de las personas y cómo toman las decisiones es fundamental para la legitimidad y efectividad de las políticas que implementan.

Por eso, la participación ciudadana, fuera de los periodos electorales, es una herramienta esencial y efectiva para involucrar a los habitantes de una comuna en decisiones que les afectan de forma amigable a sus realidades específicas. En otras palabras, los municipios tienen el conocimiento y la cercanía sobre las comunidades que le permiten ser un agente clave en acercar la toma de decisiones de forma efectiva para la realidad de cada localidad. Esto no solo es relevante para las políticas públicas locales, sino que también puede ser un vehículo para que, a través de los concejos municipales y alcaldías, los representantes locales canalicen demandas de vecinos y vecinas hacia el debate nacional.

Revisemos algunos ejemplos: las consultas ciudadanas respecto de las movilizaciones sociales en el 2019 y las medidas sanitarias en el contexto del comienzo de la pandemia, como el aislamiento o la suspensión de clases, demostraron cómo el trabajo de líderes municipales ha logrado impactar en la agenda nacional de manera propositiva.

Frente a esto, las elecciones del 15 y 16 de mayo son una gran oportunidad para conocer los compromisos de las candidaturas a las alcaldías y concejalías por la participación ciudadana y qué canales entregarán para hacer efectiva la voluntad del pueblo en la toma de decisiones. Una prueba relevante puede ser el mismo proceso constituyente chileno. Si tomamos como referencia el proceso constitucional en Colombia de 1991, observamos que los gobiernos locales fueron parte fundamental en espacios de deliberación y participación ciudadana. En ese caso, las alcaldías se encargaron de organizar e integrar las mesas de trabajo, contando con la participación de los distintos estamentos y fuerzas sociales de la región. De hecho, se registraron alrededor de 1.500 mesas de trabajo, de las cuales aproximadamente las alcaldías coordinaron 840.

Frente a un proceso histórico y tiempos muy acotados para el trabajo de la Convención, la coordinación y cooperación entre las diferentes esferas del Estado para facilitar la participación ciudadana es de suma importancia. Estamos convencidos de que los órganos locales colegiados con representación de los distintos sectores políticos, particularmente las concejalías, deben brindar y coordinar verdaderos espacios de participación donde la ciudadanía tenga un canal genuino y abierto de involucramiento en este momento que es nuestro, de la ciudadanía en su diversidad.
