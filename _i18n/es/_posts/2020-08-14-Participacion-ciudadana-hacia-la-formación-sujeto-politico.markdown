---
layout: post
title: 'Participación ciudadana, hacia la formación de sujetos políticos'
intro: 'Invitamos a Andrés Pereira de Decidim para debatir sobre la participación ciudadana y acá sus palabras.'
day: 14
month: ago
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Participación ciudadana, hacia la formación de sujetos políticos'
img: andres.png
caption:
---
***Autor: Andrés Pereira de Lucena, consultor técnico informático. Cofundador de [Decidim.org](https://decidim.org/es/)***

En estos tiempos de falta de confianza y crisis de legitimidad en los políticos y sus partidos, una de las soluciones que se están proponiendo por parte de algunas instituciones públicas es introducir nuevos mecanismos de participación ciudadana para involucrar a la ciudadanía más activamente y hacerles tomar parte de diferentes procesos de su ámbito.

La participación es la inclusión de aquellas personas potencialmente afectadas o interesadas en una decisión en particular. Es importante contar con ellas ya que permitirá beneficiarse de mejores ideas a través de la inteligencia colectiva (¿qué mejor que las personas afectadas para proponer una posible solución a un problema que conocen bien?). Además, permite la integración del conjunto de la sociedad y la oportunidad de participar en los casos de personas sin derecho a voto (por ejemplo migrantes no regularizados) que usualmente son privados de la participación en procesos electorales.

Con la llegada de las nuevas tecnologías es fundamental incluirlas dentro de los mecanismos de participación ciudadana, de cara a intentar facilitar la participación por parte de la juventud, acercarse a colectivos que no puedan trasladarse al lugar físico del encuentro o, por ejemplo, cómo estamos viviendo estos últimos tiempos, en caso de pandemias o restricciones a la movilidad, es el único canal que puede continuar ofreciendo procesos colectivos afianzando así la transformación individual en sujetos políticos.

Sobre este tema se ha impartido una charla el pasado día 21 de Julio en el marco del proyecto de ABRE Alcaldías organizado por la Fundación Ciudadanía Inteligente, en la que también se han presentado distintos recursos como punto de partida para seguir investigando, así como las herramientas más innovadoras que hay ahora mismo dentro de la industria del software con licencia libre y abierta. Acá puedes revisar la presentación completa.

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSIGPmaFCGGez0rL-HMtEuFM41otnQub1wpdmrALeoWWPc1WHW36BZrmT0CaOJLL0UDv1hiFhbPIAL9/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>


**En definitiva, y como dijo Noam Chomsky: "Internet puede ser un paso muy positivo hacia la educación, la organización y la participación en una sociedad significativa." Utilizar buenas herramientas lo hará posible, la historia está por escribir.**
