---
layout: post
title: '¿Voto obligatorio o una mejor política?'
intro: 'Chile tuvo la votación con menos participación desde el voto voluntario ¿Qué estamos haciendo mal?'
day: 17
month: jun
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: '¿Voto obligatorio o una mejor política? by @ciudadaniai'
img: 17-06-21.png
caption:
---
Por primera vez en la historia de Chile se celebró una segunda vuelta para elegir a Gobernadores Regionales en 13 regiones de Chile. Un pequeño avance en la lucha por la descentralización y la participación ciudadana en la designación de representantes locales.

A pesar de lo histórico que clamaba ser este proceso, la tarde del día domingo 13 de junio se realizó un conteo de votos extremadamente rápido y la noticia (aunque no sorpresa) de que la participación había sido baja se difundió ampliamente. Sólo el 19,5% de las personas habilitadas para votar lo había hecho, convirtiéndose en la votación con menos participación desde la implementación del voto voluntario. Las urnas cerraron a las 6 de la tarde y a las 7 pm ya conocíamos a las personas que representarán a 13 regiones de Chile los próximos cuatro años. Pero hay un problema: todas ellas fueron electas por 2.556.898 personas, de un padrón de más de 13 millones. ¿Cómo lo corregimos?

Cualquier respuesta o solución rápida a estas preguntas no apuntará realmente a la complejidad del problema. La prioridad es implementar soluciones integrales a la falta de participación, comenzando por la restauración del voto obligatorio, pero no como la única apuesta. Debemos repensar y actuar respecto de cómo la falta de legitimidad, la desconfianza en los partidos políticos y sus candidaturas, y los pocos espacios de participación han provocado esta severa desafectación en la política.

Vamos un paso atrás. De acuerdo al PNUD (2017), las personas que efectivamente votan, versus el total de la población en edad de votar es cada vez menor desde 1990. Pasando del 79% en 1992, al 45% en 2012, y al 36% en 2016. En esos tiempos, Chile contaba con voto obligatorio e inscripción voluntaria. Por tanto, este mecanismo no es la única solución. El problema es mucho más complejo y en concreto va dirigido a quienes ejercen la política y buscan ser representantes de la ciudadanía. Hoy es momento de cambiar la forma de hacer las cosas. Buscar nuevos métodos para dar a conocer ideas, propuestas y demostrar su compromiso con aquellos y aquellas que durante años no han visto cambiar o mejorar su situación y que simplemente no les dan ganas de ir un domingo a votar.

¿Qué pasa con el voto voluntario? no es extraño (por no decir frecuente) que las y los candidatos descansen en la idea de que tienen un público “fiel” y que la ciudadanía simpatizante, militante e incluso, aquellas personas que votan por rechazar enérgicamente la otra opción, emitan efectivamente sufragio, haciendo menos importante y relevante llegar al resto de la ciudadanía que no está participando. En definitiva, se vuelve una competencia de quien consigue la minoría más alta.

Chile hoy exige que las candidaturas (a cualquier cargo de elección popular) y a los partidos políticos que trabajen arduamente en recuperar su legitimidad. Es tiempo de escuchar y comprender las reales demandas ciudadanas, entregando respuestas claras con un programa sólido y coherente.

Pero esto no es todo. A la desconexión de las candidaturas y partidos, se suma otro conflicto: La falta de información. Porque a pesar de lo histórico de este momento, de acuerdo al Barómetro Regional realizado en 2019, cerca del 80% de quienes respondieron la encuesta no habían oído hablar de la futura elección de Gobernadores. Por su parte, los medios de comunicación masivos, con graves problemas de financiamiento, entregan muchas veces información confusa, centralizada y sesgada.

Y ni sumemos a esta ecuación los costos o riesgos de votar en medio de una crisis sanitaria.

Existe una responsabilidad compartida de parte de todos y todas quienes frecuentamos las discusiones políticas y debemos exigir difusión de información de calidad y descentralizada. Pero por sobre todo, existe una responsabilidad del Estado, los gobiernos, partidos, conglomerados y alianzas políticas por promover buenos candidatos y candidatas, que abran los espacios a nuevos representantes que surjan desde la ciudadanía, que generen y consoliden mecanismos de participación directa que permitan construir la agenda política en torno a las necesidades de representados y representadas. Es, a través de la suma de estos procesos, que pueden volver a aparecer, en la población, las ganas y el compromiso de votar, y más allá de esto, de participar de la política institucional (porque de la política no han dejado de ser parte).

El 50% de participación para el Plebiscito de Octubre y las múltiples formas de participación social que existen más allá del sufragio nos dejan entrever que la ciudadanía quiere ser parte. Pero quiere hacerlo en condiciones y formatos que le hagan sentido, donde pueda incidir y sentirse escuchada. Después de todo, las democracias deben ser más que un mecanismo de elección de representantes. Deben ser un sistema de gobierno que permita la real participación ciudadana, que reivindique la diferencia, que nos enseñe el poder del diálogo, del entendimiento, e incluso de ceder cuando corresponde. Un sistema que, a final de cuentas, crea mejores vidas, más justas.
