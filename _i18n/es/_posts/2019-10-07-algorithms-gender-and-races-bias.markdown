---
layout: post
title: 'Los algoritmos replican las desigualdades de género y raza ¿Por qué?'
intro: 'Nuestro proyecto Alianza A+ busca combatir las barreras a las mujeres en la tecnología.'
day: 07
month: Oct
date: 2019-10-07 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Nuestro proyecto Alianza A+ busca combatir las barreras a las mujeres en la tecnología.'
img: 07-10-19-es.png
caption:
---
Hoy el vínculo entre género y tecnología está reproduciendo e incluso amplificando el modelo del patriarcado. Por ejemplo, de todos los profesionales que trabajan en inteligencia artificial (AI) sólo el 22% son mujeres. Google y Facebook, por su parte, reportaron que las mujeres que trabajan en AI representan el 10% y 15% respectivamente. En América Latina sólo el 38% de los usuarios de internet son mujeres y en el Reino Unido las mujeres que trabajan en ciberseguridad ganan un 16% menos en comparación a los hombres.

Si consideramos la raza, el escenario es aún peor. En el año 2018 Google reportó que un 25,7% de mujeres trabajaban en puestos técnicos, pero este número bajó a [0,8% si considerábamos mujeres negras](https://diversity.google/annual-report/). La misma tendencia ocurría con otros grandes de la tecnología, como [Facebook, Apple y Microsoft](https://www.vox.com/2017/11/9/16628286/apple-2017-diversity-report-black-asian-white-latino-women-minority), evidenciando la crisis de diversidad existente en el ecosistema tecnológico e informático.

¿Y por qué hoy este tema cobra tanta importancia? Actualmente existe un rápido incremento de los sistemas y aplicaciones que automatizan los procesos, utilizando inteligencia artificial (IA). De hecho, cada vez son más los países que han creado, o están creando [estrategias nacionales de IA](https://medium.com/politics-ai/an-overview-of-national-ai-strategies-2a70ec6edfd) para promover su uso. Esto porque permiten resolver problemas de alta complejidad, facilitan la automatización y logran personalizar los procesos con el uso eficiente de los recursos.

## LAS MÁQUINAS NO SOLO OBEDECEN, TAMBIÉN APRENDEN (Y PUEDEN APRENDER MAL)

El campo de la IA es gigante y dentro de esa combinación de algoritmos está el [Machine Learning](https://en.wikipedia.org/wiki/Machine_learning) (ML), o aprendizaje de máquinas o automático. Un sistema que hoy es ampliamente utilizado y que consiste básicamente en que las computadoras aprendan. En otras palabras, el desempeño de las máquinas mejora con la experiencia. Por ejemplo, [Netflix, utiliza ML para recomendar de forma personalizada](https://www.wired.co.uk/article/how-do-netflixs-algorithms-work-machine-learning-helps-to-predict-what-viewers-will-like) a los usuarios qué ver, y estas recomendaciones se ajustan a partir de los datos que se entregan. Estos datos pueden ser explícitos, como darle like a una cierta película o serie, o, en su gran mayoría, implícitos, como pausar cierta serie después de 10 minutos o mirar una película en dos días. Toda esta información que le entregamos a Netflix, en conjunto con la de todos los demás usuarios, es el dataset, o conjunto de datos, que utiliza su sistema de recomendación para mejorar constantemente.

Sobre estos procesos existen diversos casos concretos que muestran cómo el uso de IA para automatizar procesos replica sesgos de género. Por ejemplo, el año 2014 Amazon comenzó a desarrollar un programa que, utilizando [Machine Learning](https://www.reuters.com/article/us-amazon-com-jobs-automation-insight-idUSKCN1MK08G), permitía revisar y evaluar automáticamente cientos de currículums. Este sistema aprendía de los currículums recibidos en los últimos 10 años y el desempeño de las personas contratadas en ese rango de tiempo. El 2015 la compañía se dió cuenta que este nuevo sistema ponderaba de peor manera a las mujeres para puestos de desarrollo de software u otras labores técnicas. Un reflejo del histórico dominio masculino en la industria tecnológica. A pesar de los esfuerzos, Amazon no pudo revertir este aprendizaje y desechó el programa.

Por otra parte, Joy Buolamwini y Timnit Gebru, investigadores del MIT, [evaluaron los software de reconocimiento facial de IBM, Microsoft y Face++](http://news.mit.edu/2018/study-finds-gender-skin-type-bias-artificial-intelligence-systems-0212), encontrando que todas las compañías tenían mejor performance en reconocer rostros de hombres que de mujeres, y en rostros de personas de piel clara versus en personas de piel oscura. Al analizar los sub-grupos, el reconocimiento facial de mujeres negras fue el que tuvo mayor tasa de error en las tres compañías: un 31% de error en promedio. Este resultado se contrapone con el error de menos de un 1% para hombres blancos.

Estas tasas de error son críticas. [Esta tecnología es la que hoy es utilizada por gobiernos para la detección de tendencias criminales](https://www.nytimes.com/2017/05/01/us/politics/sent-to-prison-by-a-software-programs-secret-algorithms.html). Y bien sabemos que ésta pueden usarse para generar un informe de probabilidades de reincidencia o influir en las sentencias de las personas.

Ejemplos como estos dan cuenta del rol que tiene el uso de la Inteligencia Artificial en la creación de sistemas automatizados. El hecho de que dichos sistemas sean entrenados con un conjunto de datos generados por personas, y la experiencia de éstas, implica que los sistemas aprenderán y mantendrán la subjetividad que puede venir con ellas. Lo que significa, que los sesgos existentes en el mundo offline son traspasados al online. En este sentido, se hace necesario promover estrategias que garanticen que la adopción de nuevas tecnologías en escala masiva detengan la creación de más desigualdades de género, de raza, entre muchas otras.

Esta realidad asusta y se convierte en un escenario crítico cuando pensamos que ésta será, y está siendo, la tecnología que impulsarán los Estados y que influirá directamente en las libertades y los derechos civiles de las personas. Muy probablemente determinará becas, subsidios de vivienda, permisos de migración y tendrá un rol clave en la seguridad ciudadana.

Desde Ciudadanía Inteligente, trabajamos constantemente para buscar formas en que los sesgos actuales de la sociedad no se repliquen en estos sistemas, y que estos puedan ser usados, incluso, para revertirlos. En conjunto con [Women at the table](https://www.womenatthetable.net/) (W@tt), organización de la sociedad civil que promueve la igualdad de género, luchamos para visibilizar los prejuicios existentes, hacer un llamado claro a diferentes actores para defender la transparencia y pilotear con acciones afirmativas en los algoritmos. Adherimos a la [declaración](https://www.womenatthetable.net/blog/triple-a-affirmative-action-for-algorithms) de W@tt que hace un llamado a:

* Defender y adoptar directrices que establezcan transparencia en la toma de decisiones algorítmicas.

* Incluir una variedad interseccional y número paritario de mujeres en la creación, diseño y código de toma de decisión basada en algoritmos.

* La existencia de cooperación internacional y un enfoque basado en derechos humanos en el desarrollo de estos sistemas.

Estamos en un punto decisivo de la historia. La cantidad de sistemas de toma de decisiones automatizados no tiene precedentes, y estos se están desplegando a gran velocidad. Podemos dejar que la tecnología siga poniendo barreras a las mujeres y los grupos sub-representados, o podemos utilizarla a nuestro favor. En Ciudadanía Inteligente seguiremos trabajando para desarrollar acciones afirmativas con el fin de corregir los sesgos que impiden una participación plena de las personas en las diferentes esferas de la sociedad.
