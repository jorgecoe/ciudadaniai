---
layout: post
title: 'La violencia política es un ataque permanente a la democracia'
intro: 'Exigimos medidas de protección a todos los Estados de América Latina.'
day: 23
month: junio
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La violencia política es un ataque permanente a la democracia'
img: violence.png
caption:
---
América Latina, desde su constitución, lleva consigo un proceso de expropiación y violencia contra pueblos originarios, indígenas, africanos y afrodescendientes, quienes fueron esclavizados y abandonados por el estado desde el comienzo de su constitución; la colonización. A pesar de que al día de hoy la mayoría de los países se considera “independiente” de la colonia, los efectos sociales y raciales de esta no cuentan con un “final”; la exclusión, desigualdad y violencia continúan arraigados hoy, de manera intrínseca en el tejido social de los países latinoamericanos.

Esto significa que los procesos sociales, educacionales, formativos y políticos instalados en la región mantienen la esencia y parte de la herencia de este colonialismo, el que tenía como norma los desequilibrios y asimetrías en el acceso al poder, a las instituciones, a la representación y a los derechos sociales.

A pesar de esto, Latinoamérica ha sido testigo de que gracias a las incansables luchas de los movimientos sociales de base y la determinación de algunos liderazgos, se ha podido avanzar en la representación y el reposicionamiento de los grupos históricamente menos representados y vulnerados, lo que les ha permitido el acceso a  salarios, diversificación de puestos laborales y acceso a universidades. Sin embargo, la participación en las esferas de poder ha sido breve y no ha tenido la real capacidad de penetrar en las estructuras de control tan bien establecidas por el colonialismo y por las desigualdades sociales.

Sumado a esto, las las élites económicas y políticas de América Latina y el Caribe han logrado llevar a cabo un proceso de intervención moral y política que aparenta promover y ampliar los derechos sociales, pero con un Estado muy “pequeño” como para defenderlos y garantizarlos, a la vez que existe un Estado “grande”, para aquellos procesos relacionados a condenas, penalización y muerte, que ha interpretado, además, la lucha por la igualdad como un llamado al desequilibrio social, y debe ser, entonces, enfrentado y contenido.

Estos procesos nos llevan hoy a la la materialización de la necropolítica, un proceso donde los propios estados promueven políticas que ocasionan muerte en su ciudadanía.

Se ha podido evidenciar esta tendencia en la violencia política y social contra mujeres, así como en otros grupos ya mencionados, donde la invisibilización, la no protección y el castigo estatal, se vuelve el centro de un avance neocolonial de la política latinoamericana. De hecho, de marzo a junio de 2020, en el contexto de la crisis COVID-19, la violencia contra las mujeres aumentó en un 22% en Brasil, según datos del Foro de Seguridad Pública del país. Con respecto a los residentes de los barrios marginales y la población negra, se duplicó el número de víctimas por violencia policial, incluso durante la cuarentena, añadido a que son también las mayores víctimas fatales de COVID-19.

Una situación similar se repite en el resto de América Latina. Países como Chile, Argentina, México y Ecuador han visto cómo las denuncias por violencia doméstica y violencia contra mujeres han aumentado críticamente en épocas de cuarentena. Esto no significa que la violencia desaparece al levantarse las medidas sanitarias; países como México y Bolivia, que cuentan con una legislación avanzada en  materia de cuotas de género para una mejor representación en espacios de poder, presentan crímenes, acoso y hostigamiento recurrente, que busca impedir o cesar la participación política de las mujeres, llegando en muchos casos a la violencia física e incluso en asesinatos.

Por otra parte, y en relación a personas jóvenes que residen en barrios marginales, líderes comunitarios, activistas políticos, y grupos históricamente vulnerados son víctimas de criminalización por parte del Estado, a lo largo de toda Latinoamérica, provocando condiciones precarias, curvas de inseguridad, ataques y limitaciones para la toma de decisiones. Estos grupos, en pandemia y crisis sanitaria, suman a su condición la falta de atención social, apoyo económico y acceso a la salud.

En este sentido, las prácticas entendidas como violencia política se materializan en diferentes ámbitos: física, económica, psicológica e incluso virtual, y hoy han encontrado un terreno fértil para su difusión.
Por esta razón, es **fundamental que se promuevan acciones que permitan la participación política de estos grupos.** Hoy más que nunca se debe prestar atención a las condiciones materiales, pero también sociales y de apoyo, siendo ambas parte de la infraestructura clave para una participación política segura, que no ponga en riesgo a la ciudadanía.

**Debemos enfrentar y derribar la normalización de la violencia política para así defender la democracia y promover el fortalecimiento de las instituciones públicas.**

**¡Exigimos medidas de protección a todos los Estados de América Latina!:**

Exigimos medidas de protección a todos los Estados de América Latina:
* Proyectos de ley nacionales que defiendan los marcos supranacionales sobre violencia política.

* Más apoyo en las iniciativas y organizaciones que protegen a los grupos minoritarios y a sus líderes políticos.

* Garantizar espacios de representación de grupos históricamente vulnerados en la construcción de políticas públicas.

* La reconfiguración de la fuerza estatal, con especial enfoque en disminuir la sobre-reacción.

* Creación de campañas que evidencien las desigualdades de género, raza, clase, sexualidad y territorio.

Acá puedes revisar el [video](https://www.youtube.com/watch?v=UH90uOCB71A)
