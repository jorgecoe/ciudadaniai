---
layout: post
title: 'Una alianza global para combatir la discriminación de los algoritmos'
intro: 'Iniciativa que busca revertir los riesgos de la Inteligencia Artificial y corregir sus sesgos.'
day: 08
month: Oct
date: 2019-10-07 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Iniciativa que busca revertir los riesgos de la Inteligencia Artificial y corregir sus sesgos by @ciudadaniai.'
img: 08-10-19.jpg
caption:
---
***Mientras que el gobierno de Chile inicia discusiones hacia una Política Nacional de Inteligencia Artificial, y en el mundo hay una carrera acelerada por su liderazgo, la ONG, [Ciudadanía Inteligente](https://ciudadaniai.org/) ofrece una propuesta disruptiva. Hoy lanzó la [Alianza A+](https://aplusalliance.org/), iniciativa que busca revertir los riesgos de la implementación de la Inteligencia Artificial en el sector público y rediseñarla para maximizar sus beneficios y corregir a futuro las desigualdades estructurales de género que afectan a toda la sociedad.***

En [Ciudadanía Inteligente](ttps://ciudadaniai.org/) junto a organización europea [Women@theTable](https://www.womenatthetable.net/) lanzamos la [Alianza A+](https://aplusalliance.org/). La primera iniciativa global que ofrece un listado de acción a gobiernos, empresas y sociedad civil para combatir los sesgos que reproducen los algoritmos y diseñar sistemas que corrijan proactivamente las desigualdades estructurales que afectan a toda la ciudadanía. El proyecto busca plasmar la equidad de género como principio de diseño en los sistemas del mañana.

La alianza incidirá en gobiernos y organismos privados, para que estos diseñen los proyectos de decisiones automatizadas teniendo como principio rector la equidad de género. Esto no se traducirá solamente en código auditable y transparente: permitirá también un mayor número de equipos de diseño y desarrollo mixto, más empresas lideradas por mujeres que desarrollan estas tecnologías, y el desarrollo de pilotos que probarán sus efectos en mujeres diversas. Además, la iniciativa busca que las niñas de todo el mundo, y especialmente de América Latina, reciban hoy la educación que les permita ser arquitectas de su futuro.  

Nuestra directora ejecutiva de Ciudadanía Inteligente, Renata Ávila, explicó que “esta iniciativa ofrece una respuesta concreta a la denuncia global de que los sistemas de Inteligencia Artificial son discriminatorios y además poco transparentes. La evidencia en países desarrollados ya muestra que  estos sistemas replican las desigualdades. Se ha visto en la elección para trabajos, la feminización explícita de asistentes virtuales y la exclusión sistemática de mujeres diversas de los sistemas de reconocimiento facial. Ya no se trata solamente de ética y transparencia: la exclusión de la mitad de la población pone en riesgo nuestra democracia.”

Comprometidas por la justicia social, esperamos que el proyecto Alianza A+ derive en acciones concretas para la creación de nuevas y mejores políticas públicas que permitan una participación plena de todas las personas, independiente de su raza o género, en las diferentes esferas de la sociedad.
