---
layout: post
title: 'Condenamos la represión estatal y militarización de Chile'
intro: 'Fundación Ciudadanía Inteligente condena represión estatal y rechaza militarización de Chile.'
day: 20
month: Oct
date: 2019-10-20 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Fundación Ciudadanía Inteligente condena represión estatal y rechaza militarización de Chile.'
img: 20-10-19.jpg
caption:
---

***Ante el Estado de Emergencia impuesto desde el día 19 de octubre de 2019 por el gobierno de Chile, que rápidamente se extendió de una a tres regiones del país, la Fundación [Ciudadanía Inteligente](https://ciudadaniai.org/) condena  la represión estatal,  el despliegue de fuerza militar y hace un llamado al Gobierno a revocar la suspensión de derechos y garantías, recuperar el mando y control civil del país y retirar a las FFAA de las calles para comenzar un diálogo en respuesta a la demandas de la ciudadanía.***

Hoy Chile vive la segunda jornada de Estado de Emergencia en seis regiones del país y no se descarta imponerlo en más, tampoco queda fuera de la mesa la posibilidad de renovación del Toque de Queda. El Gobierno de Chile a través de su Ministro del Interior, anunció la entrega de más potestades a la fuerzas armadas para restringir las libertades de la ciudadanía. Este escenario se da sin ofrecer un plan concreto, y sin referirse a las instancias de diálogo anunciadas en la tarde de ayer por el Presidente Sebastián Piñera.

En este sentido, la Fundación Ciudadanía Inteligente, como representante de la sociedad civil de Chile, exige al gobierno que invierta en mecanismos y protocolos efectivos para escuchar las demandas de la población. Asimismo, en la organización explicaron que el gobierno debe asignar los recursos necesarios para diseñar estrategias de manejo de conflictos sociales, evitar su escalada e involucrar a amplios sectores para resolver los problemas pendientes.

La organización, del mismo modo, demanda una activación a distintas instancias públicas, privadas y sociales, incluyendo los medios de comunicación, los centros educativos y las organizaciones de la sociedad civil organizada, para actuar a tiempo y evitar escalamientos de conflictividad social.

La directora ejecutiva de Ciudadanía Inteligente, Renata Ávila, señaló que “desde Ciudadanía Inteligente hemos observado, con tristeza, como errores del Gobierno de Sebastián Piñera y su Gabinete de Ministros rebasaron el límite de tolerancia de la ciudadanía, hiriendo profundamente su dignidad y memoria, al verse silenciados y confinados a sus casas por un toque de queda, reforzado por las Fuerzas Armadas desplegadas por la ciudad” Asimismo, la ejecutiva agregó que “este despliegue militar, en uno de los países más seguros del mundo, precisamente para contener y silenciar la protesta ciudadana, es un error que debe admitirse y revocarse de inmediato. Es violencia estatal, que conlleva responsabilidad internacional”.
