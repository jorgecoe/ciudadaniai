---
layout: post
title: 'Plebiscito en Chile: La hora de quienes faltaban'
intro: 'Ahora toca definir si los cambios serán ejecutados por quienes los impulsaron, o por las élites de siempre.'
day: 27
month: oct
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Plebiscito en Chile: La hora de quienes faltaban'
img: blogauska.png
caption:
---
*Texto escrito por Auska Ovando, coordinadora general de Ciudadanía Inteligente y publicado en [Open Democracy](https://www.opendemocracy.net/es/plebiscito-en-chile-hora-de-quienes-faltaban/) y [La Marea](https://www.lamarea.com/2020/10/28/plebiscito-en-chile-la-hora-de-quienes-faltaban/)*

La historia dirá que en el país modelo del neoliberalismo una ciudadanía hastiada cambió décadas de desesperanza por acción. Todavía queda por definir si los cambios que enfrenta Chile serán ejecutados por quienes los impulsaron, o por las élites de siempre.

El domingo 25 de octubre, un video se viralizó en las redes sociales chilenas. [Nancy, una mujer mapuche](https://www.elmostrador.cl/noticias/multimedia/2020/10/25/quiero-un-pais-mejor-mujer-mapuche-de-lo-hermida-no-pudo-evitar-emocionarse-tras-votar-en-el-plebiscito/), era entrevistada por quien al parecer es su hija, quien le pregunta qué espera de la votación de ese día. Nancy, sobre el inédito plebiscito para cambiar la Constitución que dejó Pinochet que consiguió la ciudadanía chilena, se emociona, mira al cielo, y dice: “Espero un país mejor para ti”.

Millones de chilenas y chilenos marcaron sus votos este domingo con la esperanza de que las cosas van a cambiar. Si no es para esta, para las generaciones que vienen. La opción ‘Apruebo’ a cambiar la constitución ganó con cerca de un 80% de los votos, al igual que la alternativa de que el nuevo texto sea redactado por un órgano 100% electo por la ciudadanía (y no compuesto en su mitad por actuales congresistas, como era la opción).

Los resultados repiten el mensaje con el que se movilizó la ciudadanía chilena en octubre pasado: No más abusos. En un país donde [la desigualdad tiene a quienes ostentan el poder tienen un estándar de vida nórdico, mientras que otros viven en condiciones similares a las de África](https://www.ciperchile.cl/2011/06/06/%C2%BFen-que-pais-vivimos-los-chilenos/), no más abusos no es sólo un no a las instituciones que dejó la dictadura. No más abusos es también un clamor para que, por primera vez en décadas, se escuche con fuerza la voz de quienes se les ha negado el poder en todas sus formas.

**El proceso constitucional chileno no es sólo una oportunidad para cambiar cómo funciona el Estado, es también el momento en que todas y todos quienes hemos disfrutado de los privilegios que nos ha dado el sistema le demos espacio a aquellos a quienes el sistema ha perjudicado.**

Esto significa abrir los medios de comunicación a voces que no cuentan con los marcadores tradicionales de “autoridad” ni “prominencia” que los periodistas tanto hemos elevado, pero cuya experiencia es precisamente la más valiosa para definir cómo vamos a cambiar las cosas. Significa también **asegurar que personas con trayectorias de vida diversas (sobre todo a nivel socioeconómico, de etnia, de género y de orientación sexual) sean quienes redacten y negocien nuestra nueva Constitución.**

Si el proceso constituyente parte con la sensación masiva de que quienes lo llevarán a cabo son los mismos de siempre, ¿qué tipo de esperanza podemos entregar? Desde Ciudadanía Inteligente pondremos nuestras capacidades y experiencia a disposición de la diversidad y representatividad del proceso, porque sabemos que Chile ya no puede esperar.
