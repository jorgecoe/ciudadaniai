---
layout: post
title: 'El renacer democrático de Chile está en manos de la ciudadanía'
intro: 'El proceso constituyente es una oportunidad única para discutir y construir un nuevo pacto social.'
day: 16
month: jul
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'El renacer democrático de Chile está en manos de la ciudadanía by @ciudadaniai'
img: convencion.png
caption:
---
*Texto escrito por Catalina Balla, directora de comunicaciones de Ciudadanía Inteligente y Colombina Schaeffer, subdirectora de Ciudadanía Inteligente publicado en [Open Democracy](https://www.opendemocracy.net/es/renacer-democr%C3%A1tico-chile-ciudadan%C3%ADa/)*

En Chile comenzó un proceso histórico. Impulsado por el arrojo y la valentía de la ciudadanía, este proceso único busca refundar el país y cambiar las reglas del juego escritas hace más de 40 años, por unos pocos y en dictadura. Hoy, por fin, vemos que es posible reivindicar el poder ciudadano a través de la construcción de una nueva Constitución con paridad de género y con escaños reservados para pueblos indígenas, que sirva al interés general y no a las élites de siempre.

Estamos convencidas de que es momento de que se escuche con fuerza la voz de a quienes se les ha negado el poder en todas sus formas. Miramos este camino que empieza con esperanza, y con la profunda convicción de que impulsará cambios sustantivos en las relaciones desiguales de género, el respeto al medioambiente, en el reconocimiento y garantía de los derechos, y en la forma en que se distribuye el poder en la sociedad.

El pasado 4 de julio, en el centro de Santiago de Chile, se dio el pistoletazo de salida a la Convención Constitucional. Y se hizo desde una instalación que representaba un intento de apertura y transparencia, porque los trámites se desarrollaron bajo un toldo transparente y abierto, muy distinto de los edificios de puertas cerradas a los que estamos acostumbradas al pensar en instituciones políticas y sedes de autoridades, enviando con esto una señal de que la “buena” política es posible.

Pudimos observar a distintos grupos de personas con diversas posiciones articularse, y hacerlo de cara a la ciudadanía. Observamos también conflictos y disensos. Pudimos ver cómo parte de las y los convencionales defendieron el espacio para la manifestación pública y pacífica de las demandas ciudadanas durante la ceremonia. También fuimos testigos de votaciones, negociaciones y concesiones. Y muy importante, observamos decisiones que, a diferencia de a lo que estamos acostumbradas, se tomaron con transparencia y con las puertas abiertas a todo Chile y el mundo. En resumen: observamos a personas diversas hacer política en un espacio abierto.

¿Este proceso está tomando más tiempo de lo pensado? Probablemente sí. Pero la democracia es así, es lenta. Y como sociedad nos debemos este tiempo. Sin embargo, aún quedan dudas de cómo se garantizará la participación ciudadana durante todo el proceso constituyente.

Por esto, en [Ciudadanía Inteligente](https://ciudadaniai.org/) junto [CONSTI TU+YO](https://www.constituyo.cl/), la [Iniciativa Global por los Derechos Económicos, Sociales y Culturales (GI-ESCR por sus siglas en inglés)](https://www.gi-escr.org/) y la [Fundación Friedrich Ebert (FES)](https://chile.fes.de/), buscamos promover la participación e incidencia efectiva de la ciudadanía a través de nuestro proyecto: [La Constitución es Nuestra](https://laconstitucionesnuestra.cl/), una plataforma abierta, colaborativa y colectiva que busca reinvidicar el poder ciudadano.

Porque sabemos que este proceso le pertenece a la ciudadanía, queremos visibilizar y articular propuestas ciudadanas en materia de derechos sociales, económicos, culturales y ambientales, así como de fortalecimiento democrático, conectándolas con el trabajo de las y los convencionales constituyentes.

Además, se podrá acceder a toda la información relevante sobre la Constitución, el Proceso Constituyente, la Convención Constitucional y sus mecanismos de participación ciudadana en un formato claro y amigable, así como diversas herramientas para que toda la ciudadanía pueda elaborar e impulsar propuestas.

El proceso constituyente es una oportunidad única para discutir y construir un nuevo pacto social que permita afrontar los desafíos del futuro con mayor cohesión social, fortalecimiento de la democracia, legitimidad de sus instituciones políticas y mayor ejercicio de derechos por parte de la ciudadanía.

Esto recién comienza. Queda un inmenso recorrido por delante y es nuestra tarea impulsar una Constitución escrita por toda la ciudadanía y no por unos cuantos, que acostumbraban a ser los de siempre.

<iframe width="560" height="315" src="https://www.youtube.com/embed/38HD1vXub_A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
