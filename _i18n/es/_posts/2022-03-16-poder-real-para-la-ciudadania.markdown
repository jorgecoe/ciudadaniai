---
layout: post
title: 'Poder real para la ciudadanía'
intro: 'Los Mecanismos de Democracia Directa deben entregar un poder real a la ciudadanía.'
day: 17
month: mar
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Poder real para la ciudadanía'
img: andres.png
caption:
---
***Por: Federica Sánchez, Coordinadora de Incidencia y Contenidos de Fundación Ciudadanía Inteligente.***

El estallido social, caracterizado por una grave crisis institucional y de representación sin precedentes puso de manifiesto la necesidad de profundizar la democracia en Chile dando origen al proceso constitucional. En un intento por responder a la baja participación electoral y la mínima confianza en los partidos, dentro de la Convención se han presentado distintas propuestas de Mecanismos de Democracia Directa (MDD).

Esta institución permite a la ciudadanía decidir directamente sobre una asunto distinto a elegir a una autoridad. Por ejemplo, aprobar leyes sobre pensiones, salud, trabajo o derogar leyes cuestionadas como la ley de pesca. Si bien hay elementos muy favorables para el fortalecimiento democrático, también existen riesgos que con un adecuado diseño institucional se pueden prevenir o atenuar.

Para generar efectos positivos en el sistema democrático y contribuir a la solución de la crisis, los MDD deben entregar un poder real a la ciudadanía y a grupos que no necesariamente cuentan con representación formal en los espacios de toma de decisiones. Para ello es fundamental que los mecanismos contemplen la iniciativa ciudadana y que mediante la recolección de firmas un grupo de personas pueda activarlos. Adicionalmente, es importante y necesario que la decisión final sobre el tema propuesto quede en manos de la ciudadanía mediante votación universal en lugar de depender de las decisiones del Congreso o cualquier otro órgano representativo.

El involucramiento directo de la ciudadanía en la toma de decisiones permite dar voz a expectativas insatisfechas y complementa así el modelo representativo. En última instancia, se mejora la calidad del debate legislativo, porque los parlamentarios no pueden desentenderse de las demandas ciudadanas en tanto y en cuanto la ciudadanía podría poner en marcha los mecanismos para promover leyes que no estuviesen siendo consideradas o derogar otras que no le parecieran aceptables.

Las propuestas de democracia directa que están actualmente en discusión responden a varias de estas necesidades, pero requieren de ajustes para que sean una herramienta efectiva que distribuya poder político hacia la ciudadanía. En particular, es fundamental que se reconozca la capacidad de la ciudadanía de activar y decidir sobre los asuntos propuestos de forma directa y no a través de representantes en el Congreso. Hoy más que nunca, para restaurar la confianza en las instituciones, hay que diseñar mecanismos que abran los espacios de decisión a la diversidad de la ciudadanía, tomando los resguardos para no caer en viejas prácticas que puedan implicar la captura política de los procesos.
