---
layout: post
title: 'Un nuevo ciclo para Ciudadanía Inteligente'
intro: '
Se va nuestra directora ejecutiva, Renata Ávila, y aquí les dejamos una entrevista realizada por el equipo.'
day: 10
month: ago
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Un nuevo ciclo para FCI'
img: renata.png
caption:
---
Con una enorme trayectoria en derechos humanos, innovación, tecnología y trabajo internacional para fortalecer democracias, la abogada guatemalteca, **Renata Ávila llegó a Ciudadanía Inteligente para convertirse en la primera mujer en nueve años en encabezar la dirección de nuestra organización.** Hoy después de más de dos años de intenso trabajo nos deja para abrirse a nuevos desafíos. Desde todo el equipo agradecemos su valentía, su trabajo incansable por los Derechos Humanos y por la lucha de sociedades más justas, feministas e inclusivas.

**¿Qué es lo más rescatas de tu trabajo en Ciudadanía Inteligente, Renata?**

Una de las cosas que me siento más orgullosa es la activación de profundos lazos de solidaridad y colaboración interregional. Desde la interseccionalidad, desde conexiones con otros tipos de activismo y otras miradas logramos ser la organización catalizadora y activadora de redes solidarias internacionales que trasciende los movimientos sociales y llegan a la ciudadanía. Por ejemplo, nuestro proyecto [Colectiva](https://colectiva.ciudadaniai.com/) y que aún sigue articulando a una de red de más de 100 activistas de toda la región.

Tomamos liderazgo global en el tema de la alianza para los algoritmos inclusivos, con nuestro proyecto [Alianza A+](https://aplusalliance.org/) y además fuimos reconocidas como lideresas en innovación y tecnología por [UN Women en el proceso de Generation Equality](https://aplusalliance.org/en/articles/22).

Pudimos profundizar mucho más en temas de género, justicia social, justicia racial y justicia económica. Por ejemplo, nuestro trabajo con Oxfam en el proyecto [Dataigualdad](https://dataigualdad.org/) o nuestras redes con la oficina de Brasil y la sociedad civil. Todo esto nos permitió hacernos más conscientes de nuestra posición privilegiada y nos abrimos a alianzas para aprender más y hacer mejor nuestro trabajo en esos temas tan álgidos que son el origen de todas las inequidades democráticas que vivimos hoy. Además, y muy importante, pudimos adaptar y mejorar nuestros [protocolos de género e inclusión.](https://docs.google.com/document/d/1dODbhkGMZN_KP-a5sG9e452mxOalQstcBZhhYKE4wqI/edit#heading=h.fdemtev2ig9o)

**¿Cuáles fueron tus principales aprendizajes durante la dirección de Ciudadanía Inteligente?**

El poder de las mujeres. Por coincidencia, estamos en una organización mayoritariamente de mujeres y es una experiencia completamente distinta, sin ánimo de discriminar o criticar. Cuando se da a las mujeres la oportunidad de brillar, de liderar y de tomar las riendas del trabajo los resultados son increíblemente mejores.

FCI se está volviendo un semillero de lideresas del futuro y no puedo estar más orgullosa del fabuloso equipo que hoy forma y de las distintas miradas y direcciones que la fundación va a poder tomar a partir de esto. De ser una organización de solo hombres, pasó a ser una organización de mujeres muy conscientes, responsables y creativas.

Por otro lado, mi gran reflexión es cómo podemos hacer crecer una filantropía local. Cómo podemos romper con la tendencia de tres o cuatro grandes fundaciones que están muy lejos de casa y cómo podemos hacer crecer organizaciones robustas e independientes y activas desde nuestros países. Cómo podemos trascender y nutrir una red de voluntariado comprometida que no solo aporte recursos, sino que aporte manos para la misión tan grande que nos espera de fortalecer democracias.

**¿Y el mayor reto?**

Tomé la Fundación en un momento de mucha calma y certeza institucional, donde creíamos que todo era predecible, que Chile era un país en donde no pasaba nada y que era un template para el resto de los países de la región, como un modelo de buenas políticas públicas, buenas prácticas y como una democracia fuerte. Hoy podemos confirmar que las cosas no son así. Vi una profunda desigualdad, un incremento del racismo y clasismo, una abierta actitud del gobierno a abandonar su compromiso por los Derechos Humanos y por el sistema regional multilateral de protección a los Derechos Humanos.

Durante mi mandato se vino abajo toda la lucha contra la impunidad en Centroamérica con el cierre de todas las comisiones contra la impunidad y todos los esfuerzos se vinieron abajo. Se aceleró la persecución a líderes ambientales. Se deterioró la participación de las mujeres en política en la región. Se desmantelaron organizaciones de coordinación regional. Las grandes protestas de Chile, Bolivia, Colombia y lo que pensábamos imposible: Bolsonaro electo como Presidente en Brasil. Tuvimos que dar un giro donde la tecnología se quedaba corta, que es mi área de expertise, pues no brindaba respuestas y muchas veces formaba parte del problema.

**¿Qué fue lo que más te gustó de la organización?**

Lo que más me gustó de la Fundación fue la solidaridad y el cuidado mutuo del equipo. El amor y la dedicación que le ponen a Chile y a Brasil. Es indudable que la gente que trabaja para FCI en Brasil y en Chile tiene un fuerte compromiso con la democracia. El corazón y la cabeza piensa donde pisan los pies, dice un amigo y es totalmente cierto, porque es imposible no darle prioridad al lugar donde estás y a tus circunstancias locales.

**¿Tú mayor orgullo en FCI?**

Una de las cosas de las que me siento más orgullosa es el haberles transmitido una mirada crítica de la tecnología y romper un poco la burbuja del tecnosolucionismo. Conducir a FCI a una organización que no desconecte la tecnología de lo social, de lo político y de lo personal. Ese creo que fue mi mayor aporte. Y saber que transmití la cultura donde sabemos totalmente que no todo y casi nada se resuelve con una app o un website desarrollado por personas muy homogéneas.

Cambiamos ese modelo de creer ciegamente en la tecnología, hacia procesos colaborativos incluyentes que son precisamente los que vamos a seguir explorando en todo lo que hacemos. Desde la concepción de una colaboración tecnológica y política se toma en consideración esta interseccionalidad, esta multidisciplinariedad y este trabajo colectivo que se tiene que hacer en toda organización.

**¿Cuáles serán tus próximos pasos?**

Me di cuenta que he trabajado en sociedad civil por casi 15 años y creo que ya cumplí un ciclo. Yo empecé trabajando en litigio estratégico, he pasado por todos los roles que puedes tener en sociedad civil hasta llegar a Directora Ejecutiva de una fundación. Y creo que terminé un ciclo de aprendizaje y ahora quiero iniciar uno nuevo en otro sector.

Me he dado cuenta de la gran brecha de conocimiento que tienen los gobiernos y cómo puedes tener dos roles: uno es el externo, de accountability, desde sociedad civil, de rendición de cuentas, de vigilancia. Pero creo que ha llegado el momento para mí de aportar desde lo interno. No en una carrera política, sino que asistir y compartir mis conocimientos con los gobiernos del sur global para que tomen las decisiones adecuadas en temas de comercio exterior, tecnología, feminismo y  acceso al conocimiento.

Aún no tengo un rol específico, pero esa es mi orientación: dedicar la próxima década de mi vida a contribuir al servicio público municipal, internacional, o nacional. Tratar de ayudar a gobiernos progresistas en el mundo desde las mesas de decisiones a que puedan mejorar sus tareas y entregar una mejor calidad de vida a todas y todos.

*Una vez más queremos agradecer el gran trabajo de Renata y hoy abrimos el espacio a nuevos caminos para nuestra organización. Mientras realizamos un nuevo proceso de búsqueda, nuestro equipo será liderado por Auska Ovando, Coordinadora General de Ciudadanía Inteligente y la pueden contactar en su mail: auska@ciudadaniai.org*

**En Ciudadanía Inteligente seguimos para adelante que en la lucha por mejores democracias, juntas y juntos somos más fuertes.**
