---
layout: post
title: 'Fundação Ciudadanía Inteligente condena a repressão estatal e rejeita a militarização do Chile'
intro: 'Fundação Ciudadanía Inteligente condena a repressão estatal e rejeita a militarização do Chile.'
day: 20
month: Oct
date: 2019-10-20 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Fundação Ciudadanía Inteligente condena a repressão estatal e rejeita a militarização do Chile.'
img: 20-10-19.jpg
caption:
---

***Diante do estado de emergência imposto desde o dia 19 de outubro de 2019 pelo governo Chileno, que rapidamente se estendeu de uma a três regiões do país, a Fundação [Ciudadanía Inteligente](https://ciudadaniai.org/) condena a repressão estatal, o uso da força militar e pede ao governo que revogue a suspensão de direitos e garantias, recupere o comando e controle civil do país e retire as Forças Armadas das ruas para iniciar um diálogo em resposta às demandas dos cidadãos.***

Hoje o Chile está vivendo o segundo dia do Estado de Emergência em seis regiões do país e não está descartada a possibilidade de impô-lo em mais, nem a possibilidade de renovar o toque de recolher. O Governo do Chile, através do seu Ministro do Interior, anunciou a entrega de mais poderes às forças armadas para restringir as liberdades dos cidadãos. Este cenário ocorre sem oferecer um plano concreto e sem fazer referência às instâncias de diálogo anunciadas ontem à tarde pelo presidente Sebastián Piñera.

Nesse sentido, a Fundação Ciudadanía Inteligente, como representante da sociedade civil chilena, exige que o governo invista em mecanismos e protocolos eficazes para escutar as demandas da população. A organização também explicou que o governo deve alocar os recursos necessários para desenhar estratégias de gestão de conflitos sociais, evitar sua escalada e envolver amplos setores para resolver problemas pendentes.

Da mesma forma, a organização exige a ativação de diferentes instâncias públicas, privadas e sociais, incluindo a mídia, centros educacionais e organizações da sociedade civil organizada, para agir no tempo e evitar a escalada de conflitos sociais.

A diretora executiva da Ciudadanía Inteligente, Renata Ávila, destacou que "a partir da Ciudadanía Inteligente temos observado com tristeza como os erros do Governo de Sebastián Piñera e seu Gabinete de Ministros ultrapassaram o limite da tolerância dos cidadãos, ferindo profundamente sua dignidade e memória, Além disso, a diretora acrescentou que "este deslocamento militar, em um dos países mais seguros do mundo, precisamente para conter e silenciar o protesto dos cidadãos, é um erro que deve ser admitido e imediatamente revogado". É a violência estatal,  que tem responsabilidade internacional.
