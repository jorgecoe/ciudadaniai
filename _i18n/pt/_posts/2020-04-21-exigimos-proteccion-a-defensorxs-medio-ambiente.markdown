---
layout: post
title: '¡Exijamos políticas de protección para lxs defensores del medio ambiente!'
intro: 'América Latina es la región más peligrosa del planeta para ser activista. Desde nuestra Red Colectiva exigimos políticas de protección.'
day: 21
month: Abril
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: '¡Exijamos políticas de protección para lxs defensores del medio ambiente!'
img: capa_blog.png
caption:
---

Más de 1.500 activistas ambientales han sido asesinados en los últimos 15 años. Y sólo en el 2018 asesinaron a 164 personas por defender sus tierras, es decir, cada semana de ese año mataron a tres defensores ambientales en el mundo. Lo peor es que el 51% de esas muertes se produjeron en América Latina. Hoy, en el Día Mundial de la Tierra y ante la crisis generada por el COVID-19, que ha dejado en evidencia los vestigios provocados por el sobre consumo y la sobreproducción, es más importante que nunca cuidar a quienes protegen y defienden el medio ambiente. Quienes a pesar de su rol fundamental en la lucha por resguardar los ecosistemas para y por todas las personas, se encuentran en constante peligro.

Existen distintas razones por las que estas personas defensoras y activistas se encuentran en una situación particular de riesgo en la región. Según Amnistía internacional hay tres: La primera es porque muchas de esas personas son parte de grupos vulnerados; como pueblos indígenas, afrodescendientes y comunidades campesinas. Además, Latinoamérica todavía ostenta el título de la región más desigual del mundo, lo que agrava la situación de comunidades que ya presentan un alto grado de vulneración y que, por ende, están aún más susceptibles a los ataques.

La segunda se debe a la explotación de recursos naturales en sus territorios y el control que tratan de ejercer sobre esos grupos armados en la región. Lo anterior ha llevado a la afectación del derecho al territorio de comunidades originarias la intensificación de la violencia, e impactos al medio ambiente generado bien sea por temas de contaminación o por actividades ilegales en la zona, por ejemplo: la tala ilegal.

El tercer factor es la falta de protección adecuada a personas y comunidades defensoras del medio ambiente. De hecho según la Global Witness en todos los continentes, los gobiernos y las empresas están usando a los tribunales y a los sistemas judiciales de los países como instrumentos de opresión contra quienes representen una amenaza a su poder y a sus intereses. Existiendo gobiernos que clasifican a las personas defensoras como terroristas o enemigas del Estado, o flexibilizando de las leyes nacionales para criminalizar la protesta social.

Como [Red Colectiva](https://colectiva.ciudadaniai.org/) creemos que la respuesta de los Estados ha sido sumamente débil cuando se trata de comunidades que han sido atacadas. Esto se debe principalmente a que muchos de ellos todavía ven la defensoría de los derechos humanos como algo individual y no como algo colectivo. Además, el criminalizar a las personas defensoras de manera legislativa y estatal hace que los ataques en su contra parezcan legítimos.

Hoy en el Día Mundial de la Tierra exigimos:

* Políticas públicas integrales de prevención y protección hacia lxs defensores de derechos humanos en la región que permita atacar las causas de la violencia en su contra y que garantice un espacio seguro para la ejecución de su labor.

* Poner fin a criminalización por parte del Estado hacia las y los activistas que protegen el medioambiente.

* Monitoreo de la aplicación legislativa, ejecutiva y judicial del Acuerdo de Escazú, en los países que firmaron el tratado.

* Promoción de leyes que limiten el extractivismo con fines económicos, en especial si esta provoca movilización forzada de comunidades y pueblos originarios y la devastación de los ecosistemas.

* Transparencia y disposición de la información sobre el impacto que tienen los proyectos mineros, agroindustriales y forestales.

* Creación de leyes que resguarden a aquellxs que alerten, denuncien y visibilicen crímenes contra defensores de la tierra y la naturaleza.

**Estamos viviendo tiempos difíciles y de gran incertidumbre. Pero desde [Ciudadanía Inteligente](https://ciudadaniai.org/) y [la Red Colectiva](https://colectiva.ciudadaniai.org/) nos mueve una sola certeza: juntas y juntos somos más fuertes.**
