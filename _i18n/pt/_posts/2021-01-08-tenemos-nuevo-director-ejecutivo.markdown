---
layout: post
title: 'Temos um novo Diretor Executivo!'
intro: 'O advogado Octavio Del Favero vem liderar nossa organização.'
day: 08
month: ene
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'Temos um novo Diretor Executivo'
img: octavioblog.png
caption:
---
Temos boas notícias! Soma-se à nossa organização um novo diretor executivo. O advogado chileno **Octavio Del Favero**, chegou para liderar a nossa organização com uma visão que funde direitos, participação cidadã, inovação e árduo trabalho no fortalecimento das democracias latino-americanas.

Octavio Del Favero é advogado pela Universidade do Chile com um mestrado em Política Comparada da London School of Economics e com uma vasta experiência de trabalho na sociedade civil e no sector público. Trabalhou durante quatro anos na Ciudadanía Inteligente, onde liderou a incidência legislativa no Chile, contribuindo para a reforma do aprofundamento democrático em questões de corrupção, transparência e partidos políticos. Juntamente com isso, coordenou a incidência regional através do trabalho com ativistas e redes de líderes de organizações latino-americanas.

Sabemos que as nossas democracias estão enfraquecidas, enfrentando os desafios do das desigualdades sócio-econômicas que a crise sanitária aprofundou. No entanto, estamos otimistas quanto ao enorme poder de mudança que as mobilizações de organizações cidadãs têm demonstrado em momentos chave na região. A nossa tarefa será a de promover mudanças profundas nos nossos sistemas democráticos, reivindicando o poder dos cidadãos na tomada de decisões, a fim de construir sociedades mais justas baseadas nos direitos e dignidade de todes.

Hoje começa um ciclo para a FCI e estamos convencidos de que esta nova liderança será incorporada ao grande trabalho já realizado pela equipe executiva, onde juntos trabalharão nos desafios que a América Latina enfrenta atualmente. Nesta nova fase, continuaremos atuando como agentes ativos nos processos históricos com novas iniciativas, colocando a nossa experiência de mais de 10 anos a serviço de uma maior participação, responsabilização e inclusão dos cidadãos. O nosso objectivo é fazer ouvir a voz de todes.

Saudamos calorosamente o nosso novo Director Executivo e ansiamos por lutar por melhores democracias, juntos somos mais fortes.
