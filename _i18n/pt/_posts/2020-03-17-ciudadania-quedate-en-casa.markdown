---
layout: post
title: 'Fique em Casa!'
intro: 'O que você precisa para fazer um bom trabalho remoto? Confira nossa experiência.'
day: 17
month: Mar
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Fique em casa by @ciudadaniai.'
img: coronaviruscolor.png
caption:
---
O mais importante para desempenhar um bom trabalho remoto é levá-lo a sério, mas sem enlouquecer. Estamos vivendo um momento difícil devido à propagação do Coronavírus, por isso pode ser mais difícil para nós nos concentrarmos e parar de assistir às notícias. Na [Ciudadanía Inteligente](https://ciudadaniai.org/) trabalhamos há quase 2 anos com facilidades remotas. Temos escritórios físicos no Chile e no Brasil, mas muitos trabalham de diferentes partes do mundo. É por isso que queremos dar-lhe alguns conselhos e recomendações, que para nós têm sido muito úteis, para trabalhar a partir de casa.

**AUTOCUIDADO**

**Começamos com a primeira coisa: o autocuidado. É normal não conseguir se concentrar, olhar para o infinito, ver as notícias, as redes sociais ou começar a arrumar a cozinha. Não estamos habituados a estar em casa e a trabalhar tanto. Mas aqui estão algumas dicas para te ajudar a não ficar desesperadx.**

* Levante-se cedo e tome um banho. Ficar de pijamas não é uma boa ideia.

* Reserve um espaço de trabalho separado do resto da casa (se possível). Não trabalhe da cama.
Tome tempo para tomar café da manhã, almoçar e lanchar calmamente.

* Agende horários de descanso.

* Para os tempos de descanso, recomendamos ouvir um podcast ou música. Não é uma boa ideia verificar as redes sociais. Isso só nos deixa mais ansiosxs.

* Não trabalhe através de redes sociais como a WhatsApp. Há muitas ferramentas de trabalho que podemos utilizar (mais a frente, explicaremos como usá-las) que nos ajudam a melhorar nossa concentração.

* Paciência e respeito por aqueles que estão em casa com crianças ou com mais pessoas.

* Não podemos exigir ter o mesmo ritmo do escritório em casa. Isso leva tempo.

* Nenhuma reunião durante o almoço (a fome distrai a todos).

* Dizer olá enche-nos de energia. Recomendamos começar o dia com uma chamada em equipa para saber como estão todxs, responder a perguntas e dar um impulso para o dia seguinte.

* Faça uma pausa para esticar os músculos e relaxar.Aqui está um [vídeo](https://www.youtube.com/watch?v=PBZA6Pg9VxI) que pode ser útil

* Prestar conta das nossas tarefas uma vez por semana para que o nossx chefe e/ou equipe esteja ciente do nosso trabalho.

* Não ficar ligadx 24/7 às notícias. Isso só gera stress.


**REUNIÕES E ROTINA DE TRABALHO DIÁRIO**

**Hoje é um ótimo momento para aplicar "esta reunião poderia ter sido um e-mail". Vamos tentar mostrar nossas melhores habilidades de comunicação em e-mails ou até mesmo em vídeos. Aqui estão algumas recomendações para reuniões e trabalho diário à distância:**

* Use ferramentas ou espaços virtuais para reuniões como [Hangouts meet de Google](https://gsuite.google.com/intl/es-419/products/meet/), [Zoom](https://zoom.us/) ou até [Skype](https://www.skype.com/es/).

* Todas as reuniões devem começar com um objetivo claro, juntamente com uma projeção de expectativas.

* É importante que uma pessoa modere a conversa para que ninguém seja interrompidx. Todxs deve pedir a vez para falar de forma ordenada e é essencial que uma outra pessoa esteja redigindo a ata da reunião.

* Recomendamos o uso de [Documentos Drive](https://www.google.com/intl/es-419_cl/docs/about/) (mesmo que seja da Google... nós sabemos) para que todxs possam revisar e criar documentos juntos.


* Use o [Google Calendar](https://calendar.google.com/calendar/). É uma boa maneira de ver o que a equipe está fazendo e de agendar reuniões.

* Também recomendamos ferramentas para organizar tarefas como [Asana](https://asana.com/es), [Todoist](https://todoist.com/es/), [Trello](https://trello.com/es), [Miro](https://miro.com/?utm_source=google&utm_medium=cpc&utm_campaign={_utmcampaign}&utm_term=miro&utm_content=421787753483&xuid=EAIaIQobChMIj76B__-f6AIVgg6RCh26yQSXEAAYASAAEgKeefD_BwE&gclid=EAIaIQobChMIj76B__-f6AIVgg6RCh26yQSXEAAYASAAEgKeefD_BwE), entre outras que nos ajudam a rever o que temos pendente, além de priorizar e delegar tarefas aos parceiros.

**NÃO SE ISOLE**

Há muitas salas virtuais e livres onde toda a equipe pode estar online ao mesmo tempo como [Slack](https://slack.com/intl/es-cl/lp/three?utm_medium=ppc&utm_source=google&utm_campaign=d_ppc_google_latin-america-carribbean_es_brand-hv&utm_term=slack&ds_rl=1249094&gclid=EAIaIQobChMIoKv2zoOg6AIVkIeRCh3InwXYEAAYASAAEgKas_D_BwE&gclsrc=aw.ds), [Riot](https://about.riot.im/), [Discord](https://discordapp.com/) e até o já mencionado  [Hangouts meet de Google](https://gsuite.google.com/intl/es-419/products/meet/). É importante que todxs se sintam acompanhadxs. Se estamos todxs conectadxs é mais fácil seguir a nossa rotina diária de trabalho. É importante evitar que os canais de contato sejam as nossas redes sociais, pois isso gera distração e muitas vezes atrapalha o rendimento do trabalho.

Nós da [Ciudadanía Inteligente](https://ciudadaniai.org/) sabemos que não é fácil. Foi difícil para que conseguíssemos chegar ao ponto de nos sentimos confortáveis com o trabalho remoto, mas agora adoramos. Nos permite ser mais flexíveis, autônomxs e ter a oportunidade de trabalhar a partir de qualquer parte do mundo. Às vezes pode ser cansativo, mas lembre-se que você não está só. Toda a sua equipa (e o mundo) , que tem a oportunidade, estão trabalhando de casa.

Para fazer democracias melhores, precisamos de pessoas saudáveis #StayHome #QuédateEnCasa #FiqueEmCasa.

Outras fontes: [https://staythefuckhome.com/](https://staythefuckhome.com/)
