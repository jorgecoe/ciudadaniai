---
layout: post
title: 'La democracia se fortalece ejerciéndola'
intro: 'Chile vive un momento histórico: la ciudadanía logró lo imposible para implementar una real democracia.'
day: 15
month: Nov
date: 2019-11-15 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La democracia se fortalece ejerciéndola'
img: 15-11-19.png
caption: Fotografía por Carlos Figueroa
---

Comenzó como una protesta estudiantil ante el aumento de la tarifa del metro. El viernes 18 de octubre del 2019, ante el llamado a una evasión masiva que fue atendida por las y los estudiantes y también la ciudadanía de forma crecientemente masiva, las cosas cambiaron para Chile y no tuvieron vuelta atrás. El gobierno reaccionó, como lamentablemente suele hacerlo: reprimiendo las protestas, criminalizando a quienes ejercían el derecho a protestar y sin capacidad de distinción entre quienes se manifestaban de manera pacífica versus los hechos de violencia aislados. Al sentir que perdía el control de la situación, y acudiendo a las consignas de “mano dura” y “necesaria restauración del orden a toda costa”, el presidente Sebastián Piñera decidió, como no se hacía desde tiempos de la dictadura, [utilizar la fuerza](https://ciudadaniai.org/crisis) y la violencia como forma de resolver lo que era un [problema político](https://ciudadaniai.org/chile).

Desde entonces han pasado cuatro semanas. 28 días de esperanza, solidaridad, encuentro, masividad, creatividad, movilización, insistencia y resiliencia. Y en un día histórico, el viernes 25 de octubre, más de 1,5 millones de personas se dieron cita en la emblemática Plaza Italia - que hoy varias voces quieren nombrar Plaza de la Dignidad, como forma de hacer justicia a los eventos recientes y su trascendencia histórica -, así como en las regiones del país, en una manifestación nacional pacífica, masiva, familiar con un único objetivo: crear juntos y juntas un Chile más justo.

Sin embargo, también fueron semanas de profunda oscuridad, temor, desesperanza, angustia e incertidumbre. Vimos a las fuerzas militares desplegadas en las calles. Vivimos bajo el rigor del toque de queda durante seis días. Para muchos afloraron recuerdos negros de los tiempos de la dictadura. Muchos conocieron por primera vez lo que se vive en territorios y comunas que hoy no gozan del derecho a vivir en paz. Se ejerció violencia sistemática contra la ciudadanía, a manos del Estado. El costo fue alto: cientos de personas detenidas, heridas, violadas, torturadas y mutiladas. También 22 personas que perdieron la vida. Un costo altísimo que pagar para sostener una demanda clara y transversal: el sistema actual NO funciona. Es injusto y nos entrega una sociedad profundamente desigual y violenta, sin dignidad, donde la verdadera democracia no es posible.

El Ejecutivo y la clase política demoraron en hacer lo que están convocados a hacer: gobernar y resolver los problemas desde la política, la política de verdad. El nivel de deslegitimación y desconfianza ya es muy alto. Hoy el gobierno cuenta con un 15% de aprobación. Después  de largas negociaciones, el día en que se cumplía un año del asesinato del comunero Mapuche Camilo Catrillanca a manos de fuerzas policiales, dieron luces de un posible acuerdo. Estaba claro que la ciudadanía no iba a cesar en sus demandas, que la violencia como respuesta política no funciona, y que solo atrae más violencia, en una escalada inédita que dejó lugares emblemáticos así como áreas de comercio y residencia destruidos a lo largo de todo país.

Anoche, ya casi de madrugada, conocimos el resultado de estas negociaciones: un “Acuerdo por la Paz Social y la Nueva Constitución”. Un acuerdo histórico, pero ante todo, un logro histórico de la ciudadanía. Desde Ciudadanía Inteligente consideramos fundamental que el proceso que recién se inicia se de en un marco de respeto irrestricto de los derechos y garantías fundamentales de las personas. Esto significa asegurar que las violaciones a los Derechos Humanos ocurridas en estas cuatro semanas no queden impunes. Recién sobre esa base de reparación se podrá empezar a construir un proceso realmente democrático.

¿Cómo funcionará este acuerdo? Primero con un plebiscito que dará paso a un cambio constitucional. El cambio se podrá realizar a través de dos mecanismos:

a) Convención Constituyente (equivalente a una Asamblea Constituyente), donde la ciudadanía elige representantes, quienes escriben una nueva Constitución y una vez finalizada se disuelve.
b) Congreso Constituyente, compuesto en un 50% por ciudadanas y ciudadanos, y en un 50% por miembros del actual Congreso Nacional.

Serán 155 asambleístas en ambos casos, y las decisiones estarán sujetas a un quórum de ⅔ (66,6%). En caso de no existir un acuerdo NO se vuelve a la Constitución de 1980, sino que simplemente no será materia de la nueva Constitución. El proceso culminará con un plebiscito ratificatorio y donde el voto será obligatorio.

El plebiscito constituyente (para definir la forma que tomará el grupo de personas que escribirán la nueva Constitución) se realizará en abril de 2020. Ese mismo año, en octubre, y junto con las elecciones municipales, se realizará la elección de delegadas y delegados. Aún no se han establecido las medidas para que la Convención o Congreso constituyente, en su conformación, sea realmente inclusivo y representativo, es decir, establecer cuotas para mujeres, minorías sexuales, pueblos originarios, entre muchas otras. El órgano constituyente sesionará por nueve meses, y podrá ser prorrogable sólo una vez y por tres meses más.

Hoy más que nunca nadie puede “irse para la casa”. Existen millones de interrogantes sobre cómo se llevará a la práctica el proceso iniciado. En éste será fundamental el rol de la ciudadanía y la sociedad civil para fiscalizar, participar, exigir rendición de cuentas, educar y movilizar. Como organización pondremos todas nuestras herramientas a disposición para apoyar desde nuestra experiencia un proceso que puede comenzar a transformar a Chile en un país más justo, inclusivo y sostenible, en una verdadera democracia para todos y todas. Porque saben cabros, esto sí prendió.
