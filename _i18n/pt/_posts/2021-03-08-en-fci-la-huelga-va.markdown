---
layout: post
title: 'Feminismo: movimento social mais relevante da história contemporânea'
intro: 'Nossa diretora Eglé Flores escreve sobre as lutas do feminismo e como, juntas, faremos reais mudanças.'
day: 08
month: marzo
date: 2020-03-21 12:00:00 -0300)
categories: Invitación
highlighted: false
share_message: 'En FCI la huelga va'
img: huelga.png
caption:
---
*Convidamos nossa diretora Eglé Flores para escrever uma coluna em comemoração ao Dia Internacional da Mulher. Aqui estão suas palavras.*

Os tempos mudam.

Hoje não nos dirigimos aos que nos governam, mas lhes agradecemos o espaço para honrar nossas mortas. Eu gostaria que eles tivessem a mesma proatividade e urgência para fazer cumprir as leis que nos protegem. Hoje não vamos desperdiçar nossas palavras com eles.

Hoje nos dirigimos aquelas que simbolizam a luta feminista. Os tempos exigem isso. Nos voltamos para nós para relembrar de que essa luta vai muito além de nós mesmas.

Companheira, se você acha que a violência contra a mulher tem origem no sistema patriarcal, você precisa ampliar o panorama. Fomos ensinadas a entender o mundo de forma compartimentada. Cada questão em sua caixinha e devidamente rotulada: racismo aqui, nacionalismo ali; a luta de classes aqui, a xenofobia ali. Como se as demandas históricas pela liberdade dos corpos não estivessem conectadas.

A realidade é mais complexa do que isso. Os sistemas que oprimem as mulheres – e muitos homens – são múltiplos e se sobrepõem. São corpos de mulheres racializadas, atravessadas pelo colonialismo e exploradas pelo capitalismo neoliberal. Existem corpos despojados de seus territórios e territórios que existem para além dos mapas. Algumas de nós demoramos para enxergar isso devido à falta de perspectiva e referências. Fomos educadas para ter uma memória curta, forjada por livros que mostram uma história única e limitada, narrados por quem descobriu mundos que não precisavam ser descobertos, muito menos colonizados.

Hoje sabemos que nossas experiências também são válidas. Temos mulheres que narram realidades que se entrelaçam com nossos territórios. Hoje valorizamos as palavras de María Lugones, Ochy Curiel, Yuderkys Espinosa, Aura Cumes, Silvia Rivera Cusicanqui, Leydy Pech, Rita Segato, Yasnaya Elena Gil, Gladys Tzul Tzul, Marielle Franco e muitas outras.

A luta feminista é o chamado para não abandonar a resistência de 500 anos para muitas e para o acordar de outras. É uma luta que não é uma e não é única: é uma luta de lutas, que conecta as narrativas dos feminismos negros, descoloniais, periféricos, populares, indígenas, trans. Os feminismos construídos a partir das experiências de mulheres, movimentos sociais e academia nesses territórios, que hoje nos permitem situar na complexidade global a partir de nossas próprias experiências.

Recentemente ouvi [Angela Davis](https://twitter.com/BlackQueerTH/status/1275115258697433088?s=20) dizer que “as pessoas presumem que, quando você se envolve em práticas feministas, você se concentra apenas nas questões de gênero ou apenas nas mulheres. Para mim, feminismo é uma metodologia, uma forma de pensar o mundo. É um método de organização – nos convida a sermos inclusivos. Para ser inclusivo, entendendo conexões e relacionamentos. O feminismo garante que ninguém fique para trás.” E sim, ser feminista é muito mais do que quebrar o pacto e jogar fora o patriarcado. Ser feminista é se declarar anti-racista, anticolonial; é agir para quebrar o pacto de raça e classe. Ser feminista é lutar por nós mesmas, abolindo nossos próprios privilégios, porque o poder entre poucos também não funciona.

Os feminismos representam o movimento social mais relevante da história contemporânea. Também podem ser a oportunidade de desconstruir sistemas político-econômicos que ameaçam a vida e a oportunidade de forjar democracias que respondam às exigências sociais e ambientais de nosso tempo. Da fina trama dos feminismos e de suas lutas, podemos imaginar um sistema alternativo que garante uma vida digna para todos e reconhece a autodeterminação das nações sobre seus territórios. Tal sistema já está presente, circulando na imaginação de muitos que sabem que [um mundo ch'ixi é possível](https://www.facebook.com/gislatino/posts/descarga-gratisun-mundo-chixi-es-posible-ensayos-desde-un-presente-en-crisis-sil/2503036926381758/).

Serei breve: nada tem que ser do jeito que é. Este é o momento histórico que estamos vivendo; vamos assumi-lo coletivamente e no plural.
