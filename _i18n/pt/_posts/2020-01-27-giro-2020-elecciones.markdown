---
layout: post
title: 'Um giro para construir eleições mais participativas e representativas no Brasil.'
intro: 'Em breve GIRO2020. Nossa iniciativa que fará parte de todo o processo eleitoral no Brasil. Não fica de fora!'
day: 27
month: Ene
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Em breve GIRO2020. Nossa iniciativa que fará parte de todo o processo eleitoral no Brasil.'
img: giro_blog.png
caption:
---
Nós da [Cidadania Inteligente](https://ciudadaniai.org/pt/) chegamos no Brasil em 2017 com a missão de contribuir em uma das democracias mais complexas e desafiantes da América Latina. De nosso escritório do Rio de Janeiro denunciamos e concentramos nossos esforços nos acontecimentos dos últimos anos que são um foco de preocupação em matéria de degradação democrática em todo mundo. Violência política, abuso de poder, cerceamento da liberdade de expressão e aumento da desigualdade social contaminam o ambiente político  e enfraquecem o impacto da cidadania nos processos políticos. Diante disto, o ano 2020 marca o início de um novo ciclo eleitoral municipal no Brasil e com ele uma nova oportunidade para construir o futuro que queremos para as nossas cidades.

Por isso, criamos o projeto **GIRO 2020**, uma proposta da [Cidadania Inteligente](https://ciudadaniai.org/pt/) em parceria com a  [Casa Fluminense](https://casafluminense.org.br/) para que as eleições sejam mais participativas e representativas, com uma agenda de políticas públicas territorializada e alinhada com as necessidades da cidadania. Muito além do voto, nosso giro é uma jornada que começa no ano eleitoral, vai cruzar o dia de votação e depois avançar após o resultado das urnas, de olho na gestão das nossas cidades.  

Em fevereiro iremos iniciar uma série de encontros com a sociedade civil do Rio de Janeiro onde vamos conversar sobre as mudanças recentes no nosso país e os desafios que devemos enfrentar para fortalecer espaços de engajamento e redes de acompanhamento em nossas cidades. Com os aprendizados desta movimentação e em diálogo com mais 100 de organizações e coletivos iremos consolidar uma  agenda com propostas concretas para os principais desafios enfrentados pelas cidades do Rio de Janeiro.

Comprometidos em aumentar a incidência das demandas da cidadania no período eleitoral e pós eleitoral iremos realizar processos formativos com mais 100 candidaturas além de construir um processo coletivo de mobilização territorial e monitoramento de proposta com foco em uma rede treinada de 50 lideranças territoriais.

Com o **GIRO 2020** convidamos a cidadania a permanecer unida e forte, para reverter retrocessos de direitos e disputar vidas mais dignas em nossas cidades. Por que sim, nós não desistimos.
