---
layout: post
title: 'Construir confianza después de la guerra'
intro: 'Implementamos tecnología para reparar los vínculos que quebró la violencia en Kosovo'
day: 11
month: Dic
date: 2019-12-11 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Construir confianza después de la guerra by ciudadaniai'
img: 11-12-19.png 
caption:
---

A inicios de este año la organización [New Social Initiative](http://newsocialinitiative.org/) (NSI) nos contactó con un desafío. En mayo de 2018 se desarrolló el Kosovo Trust-Building Forum, un encuentro de más de 120 representantes de diversos sectores del país, como la sociedad civil, medios de comunicación, juventudes y gobiernos locales. Fue organizado por UNMIK (la misión de Naciones Unidas para Kosovo), y su objetivo fue intercambiar perspectivas y experiencias para construir confianza en un país en el que los conflictos étnicos son profundos, y han ido acompañados de violencia descarnada. Ese encuentro generó la tarea de implementar una serie de recomendaciones y, muy importante para la construcción de confianzas, crear una plataforma pública para hacerles seguimiento.

Aceptamos el desafío presentado por NSI y UNMIK, y comenzamos a construir un sitio web para desplegar de forma transparente y amigable el avance de 135 recomendaciones con 158 iniciativas asociadas que se implementarán en los distintos municipios de Kosovo. Estas medidas son de distintas áreas, como gobernanza, acceso a la justicia, educación, creación de confianza inter-religiosa, empoderamiento económico, medios de comunicación y educación.

A 20 años del fin de la guerra, muchas organizaciones (como NSI) trabajan día a día para crear lazos entre  la mayoría serbia y la minoría albanesa. Al día de hoy, muchos viven en riberas opuestas del mismo río y nunca han tenido contacto con integrantes del otro grupo étnico. Las iniciativas y las recomendaciones del proyecto buscan avanzar paulatinamente en ese acercamiento, construyendo también las bases para lograr paz social y una democracia fuerte.

Del desarrollo del proyecto aprendimos que en estos casos la creación de contenidos web tiene un fuerte componente político, en el que hay que asegurar neutralidad en el lenguaje y sus traducciones. También pusimos mucho énfasis en un diseño amigable y alegre, con una navegación simple e intuitiva, para que usuarias y usuarios puedan navegar cómodamente y enfrentarse sin tensiones a un tema que ha sido históricamente complejo.

Estamos muy felices con el resultado del proyecto, y de haber colaborado con una organización que, como la nuestra, está liderada por mujeres, y busca mejorar las democracias en contextos de menos privilegios y recursos que los que existen en otras regiones.

Puedes revisar el sitio en [kosovotrustbuilding.com](kosovotrustbuilding.com)
