---
layout: post
title: 'COP 25: Between Debacle and Opportunity'
intro: 'The governments of the world have not taken charge of climate change. What do we do?'
day: 7
month: Jun
date: 2019-06-08
categories: News
highlighted: false
share_message: "COP 25: Between Debacle and Opportunity by @ciudadaniai."
img: 07-06-19.jpg
---
***Column written by:***
***Ezio Costa, executive director Fiscalía del Medio Ambiente Column ***
***Colombina Schaeffer, deputy director Fundación Ciudadanía Inteligente.***
***Column published on [El Mostrador](https://www.elmostrador.cl/noticias/opinion/columnas/2019/06/11/cop-25-entre-la-debacle-ambiental-y-la-oportunidad/)***

Modern society is currently experimenting a climate change crisis that is putting life as we know it at stake like never before. That being said, as we reflect and discuss this issue, rarely do we address different types of related questions. For example, we don’t ask  who the decision-makers are, much less how they make them, regarding the size of the challenge. Nor do we ask ourselves what our place is, as citizens, that is, if we even have a place beyond individual actions with doubtful global impact unless they are on an unprecedented massive scale.  Today we are discussing the climate change crisis, but in the future this can be any global challenge that requires action from all parties involved simultaneously. The classic problem of collective action, this time on a global scale, is a complexity never seen before.

The system that governs the decisions about how we face climate change is found in the UN, through [the United Nations Framework Convention on Climate Change](https://unfccc.int/es) (UNFCCC). While it is far from perfect, this system is the closest we have to any sort of global parliament, whose mission is to address a problem that endangers life on Earth as we know it. With all its complications, and being a voluntary system, since 1992 it exists and it works. It has been governed by agreements, including the Kyoto Protocol, which was recently replaced by the Paris Agreement, which will come into effect next year.

This system has faced successive crises and challenges, but today it presents a small yet important opportunity. Here’s why. The end and failure of the Kyoto Protocol implied a profound change to the governance structure of the global system to combat the climate crisis, catalyzing the new system based on the Paris Agreement. Under this framework, national governments are the ones called upon to establish minimum commitments to ensure that the average temperature of the Earth does not increase beyond 1.5-2°C by 2100, compared to the pre-industrial period. While this distributes responsibility from global to national entities, it opens up opportunities for citizens, especially organized and articulated, to influence and participate in pressuring their own governments to make ambitious commitments.

So far, the global political class has not managed to deal with this problem. The crisis becomes even more complex if we consider that expert knowledge, as well as technological solutions, exist and are known and applicable. Rather, we are facing a political-cultural problem. Political, if we consider interests and balances of power at different levels for different actors, and cultural if we understand that there is a predominant way of life associated with a myriad of cultural parameters, driven by globalization, which must be modified.

With the Paris Agreement, we can understand that sovereignty over decisions about the climate crisis returns, in a way, to citizens and national democracies (with all their strengths and weaknesses). This presents a key opportunity for global citizenship, and in our case, for Latin America, to be heard regionally and globally, in order to pressure their local respective national governments to achieve ambitious goals in their respective NDCs (nationally determined contributions).

As a region, Latin America is not only vulnerable to the climate crisis, if we consider that both the tropics, the Amazon and the coastlines are vulnerable to the changes that are coming, but also there is a perspective of the possibilities of adaptation and resilience. [A significant number of countries in the region have a medium-low level of preparation and adaptation to this phenomenon](https://gain.nd.edu/our-work/country-index/rankings/). Generally, this level of preparation goes hand in hand with the socioeconomic conditions of the countries, with the most affected being the people that are already vulnerable at other levels.

It is ambitious and perhaps not entirely realistic, but it is no less possible to do something about it. For this reason, it is important to increase visibility of this window of opportunity, so that in some way empowered citizens can finally speak to a crisis that threatens everyone equally. Power is no longer as distant as before, instead a bit closer. Imagine then, a process of regional articulation (different civil societies of the world grouped by region) where pressure is applied jointly and globally, but from a national perspective.
