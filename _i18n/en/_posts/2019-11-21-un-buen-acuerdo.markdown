---
layout: post
title: 'La nueva Constitución la escribimos entre todxs'
intro: 'Una oportunidad histórica, pero debemos vigilar desde muy cerca todo el proceso.'
day: 21
month: Nov
date: 2019-11-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'La nueva Constitución la escribimos entre todxs'
img: 21-11-19.jpg
caption: Fotografía por José Miguel Araya
---

Un paso histórico y fundamental se ha dado para nuestra democracia con el acuerdo alcanzado el pasado viernes entre parlamentarios de gobierno y oposición para dar inicio formal al proceso constituyente.

Hace menos de un mes atrás, la viabilidad de iniciar un proceso constituyente y poner definitivamente fin a la Constitución de la Dictadura era prácticamente nula. Sólo es posible que hoy estemos hablando de esto gracias a la movilización masiva de la ciudadanía que ha empujado en las calles y a través de la protesta cambios estructurales para mejorar la calidad de vida de las mayorías y poner fin a las injusticias que se vienen reproduciendo durante décadas.

Dado el origen de la demanda, hay mucho escepticismo, desconfianza y legítimas dudas sobre este acuerdo alcanzado entre partidos políticos y congresistas. ¿Cómo sabemos que no se trata de una impostura y que no son sólo anuncios grandilocuentes que no buscan cambiar nada? Para responder esta pregunta, debemos mirar el contenido mismo del Acuerdo, analizar las oportunidades que se abren y los flancos que aún quedan por resolver.

**Lo bueno**

Lo primero es que necesitábamos de un acuerdo. Para poder iniciar un proceso constituyente formal e institucional, requerimos de una reforma constitucional que permita dar inicio al mismo. De esa forma, es imprescindible contar con el apoyo de distintas fuerzas políticas para desarrollar un proceso de calidad y legítimo.

Lo mejor que tiene el Acuerdo es que se pondrá fin a la Constitución de Pinochet. Si la ciudadanía así lo decide, nuestra democracia estará libre de las amarras y restricciones que han impedido su desarrollo pleno y participativo. La constitución que nos rige, fue pensada para reducir la influencia de la ciudadanía y el poder de las instituciones representativas para conservar el modelo de sociedad que la Dictadura concibió. Con una nueva Carta Fundamental, esas restricciones se acabarán.

El proceso acordado es, además, inéditamente participativo para nuestro país. Con un plebiscito de entrada para decidir si queremos nueva constitución o no y a través de qué mecanismo (incluyendo la Asamblea Constituyente, que ahora se llama Convención Constituyente). También podremos elegir directamente al 50% o totalidad de las personas delegadas en la Convención. Finalmente, un plebiscito ratificatorio con voto obligatorio para que expresemos si estamos satisfechos o no con el resultado del trabajo realizado.

En resumen, el Acuerdo da inicio formal al proceso constituyente impulsado por la ciudadanía y todas las opciones están sobre la mesa. Ahora nos toca decidir.

**Las dudas**

De todas formas, algunas dudas han surgido sobre lo acordado y otros aspectos siguen abiertos y deben ser resueltos de forma tal de elevar al máximo las oportunidades de participación e incidencia de la ciudadanía evitando la captura del proceso por actores específicos.

Primer punto que se ha levantado es respecto al quórum de 2/3 que se exigirá para que algo sea incorporado en la Nueva Constitución. En Chile tenemos un justificado trauma con las supra mayorías ya que han permitido que demandas de cambio anheladas durante años sean bloqueadas por grupos minoritarios. Esto es resultado directo del diseño constitucional de Jaime Guzmán y es algo que no se reproducirá en el debate en la Convención.

Esto es gracias a que el trabajo se hará sobre una “hoja en blanco”, es decir, sin texto previo que nos limite o amarre. En caso de que la Convención no alcance un acuerdo en algún tema, simplemente ese aspecto no quedará regulado en la Constitución y deberá ser resuelto en una ley ordinaria con la mayoría de los votos en el futuro parlamento. Junto con eso, el quórum promueve consensos amplios, evitando que una minoría se imponga sobre los demás como ocurre actualmente. Esto también corre para los quórum de reforma de la Nueva Constitución, por lo tanto, ningún sector podrá establecer requisitos de reforma exagerados para puntos que no sea altamente consensuados.

La única excepción a esto deberían ser los compromisos internacionales en materia de DDHH que ha adquirido el Estado de Chile. Estos tratados tienen la particularidad de versar sobre asuntos propios de una Constitución y que su no consideración podrían comprometer la responsabilidad internacional del país. Es por eso, que en caso de no haber acuerdo en aspectos de DDHH, [deben regir los tratados vigentes](https://radio.uchile.cl/2019/11/18/el-debate-de-la-regla-de-los-23-y-los-dd-hh/)

Otro aspecto relevante es el sistema de inscripción de candidaturas y conformación de listas. Sabemos que regirá un sistema de asignación de escaños que será proporcional. Sin embargo, el detalle de este sistema debe garantizar una competencia igualitaria entre candidaturas de partidos políticos e independientes. Para eso se deben permitir la conformación de listas de independientes y no poner requisitos excesivos para la inscripción de sus candidaturas. Otra opción sería que se someta a las candidaturas de militantes a los mismos requisitos que independientes, sin ser favorecidos con una inscripción “automática” como en otro tipo de elecciones. Lo mismo corre para el sistema de financiamiento de campañas, el cual debe garantizar competencia equitativa entre candidaturas, sin favorecer a partidos. Esto es especialmente sensible dado que la elección será en paralelo a las municipales y de gobernadores donde los partidos sí tendrán un rol protagónico y acceso a recursos.

**La ciudadanía de nuevo**

Este proceso tiene su origen en la demanda y movilización de la ciudadanía. No porque se haya alcanzado un acuerdo en el Congreso el proceso nos deja de pertenecer y nuestro rol sigue siendo fundamental en diversos aspectos. A continuación sólo algunos que creemos deben ser recogidos con urgencia por la academia, organizaciones y ciudadanía en general:

*  Como se ve, la ley que regule los plebiscitos y elección será fundamental para que todos los flancos abiertos se resuelvan de la mejor manera y en favor de la participación e inclusión. Para ello es fundamental que como ciudadanía nos mantengamos alertas al debate y exijamos reglas que impidan la captura del proceso. Academia y organizaciones tienen un rol en desarrollar propuestas concretas para cada aspecto y hacerlas presentes en el debate legislativo que se viene.

*  Desde ahora en más, el debate constitucional y sobre el plebiscito estará más instalado que nunca en medios de comunicación. Es fundamental contar con un Observatorio de Medios que nos de información acerca de cómo se está cubriendo las distintas posiciones y qué oportunidades se están dando a grupos menos representados en el debate. Los medios tienen un rol fundamental en garantizar la inclusión y evitar la invisibilización de ideas, posiciones y personas y necesitamos fuentes de información confiable y con perspectiva de género y clase sobre cómo están desarrollando su labor.

*  Lo mismo corre para el mundo de las RRSS. Es claro que cada vez están jugando un rol más importante en el debate público y los riesgos asociados a ello son muchos debido a la falta de regulación de las plataformas y el uso indiscriminado de información falsa o mecanismos de manipulación por parte de quienes tienen recursos para ello. Vamos a necesitar a una sociedad civil vigilante para identificar, denunciar y combatir esas prácticas a través de un Observatorio de RRSS.

*  El proceso considera como principal medio de participación el voto a través de plebiscitos y elección. Lamentablemente, tenemos suficiente evidencia que la participación electoral está fuertemente determinada por aspectos socioeconómicos y de edad que deben ser combatidos para asegurar un proceso representativo en sus distintas etapas. El esfuerzo consciente y estratégico de todas las organizaciones y personas en la promoción del voto durante estos meses será clave.

*  Por último, nuestra participación no se puede limitar solamente a votar. Nuestros cabildos, reuniones y conversaciones no deben terminar. Para ello, también necesitamos que organizaciones asuman la misión de mantener vivas las conversaciones, especialmente en los espacios y con personas tradicionalmente excluidas. Necesitamos más cabildos feministas, de pueblos originarios, en regiones, en campamentos, poblaciones y donde niños, niñas y adolescentes tengan un rol protagónico. Idealmente, esa participación se debe desarrollar con marcos metodológicos compartidos que permitan a personas expertas de la academia agregar la información y entregar insumos al debate constitucional.

En otras palabras, esto recién comienza y en los detalles se jugará si tendremos un proceso y Nueva Constitución plenamente democrática y sin trampas o no. Para que el resultado sea el mejor posible, deberá ser la ciudadanía, una vez más, la que marque la diferencia y muestre el mejor camino, así ha sido durante estas movilizaciones y así seguirá siendo durante todo este proceso que se abre.

Por último, es imprescindible que todo este proceso sea desarrollado en paralelo a la búsqueda de verdad, justicia y reparación para las graves vulneraciones a los DDHH de las últimas semanas. Sin sanciones claras a las personas responsables, no será posible alcanzar un nuevo pacto que entregue paz al país. Nadie puede pretender que tengamos que escoger entre participación y respeto a los DDHH, simplemente no hay democracia posible sin justicia y se debe garantizar que no habrá impunidad, cualquiera sea la persona responsable.
