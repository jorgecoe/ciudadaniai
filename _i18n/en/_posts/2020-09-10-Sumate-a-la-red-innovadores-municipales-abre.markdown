---
layout: post
title: 'Súmate a la Red de Innovadores Municipales ABRE'
intro: 'Una comunidad de acción y aprendizaje para crear gobiernos locales más abiertos y participactivos.'
day: 10
month: sept
date: 2020-03-21 12:00:00 -0300)
categories: Proyecto
highlighted: false
share_message: 'Súmate a la Red de Innovadores Municipales ABRE'
img: red.png
caption:
---
En América Latina existen cerca de 17 mil gobiernos locales y el 77% de la población dice no confiar en su municipios, según LAPOP 2018-2019. En este contexto, lanzamos la Red de Innovadores Municipales ABRE. Una oportunidad para que todos los funcionarios públicos se inscriban de manera totalmente gratuita para recibir formación y certificación en contenidos, metodologías y herramientas sobre apertura, innovación pública y gobierno digital de manera simple y amigable.

Nuestro objetivo se centra principalmente en la construcción de una comunidad de acción y aprendizaje diseñada para funcionarios y representantes municipales comprometidos con la participación ciudadana y la apertura gubernamental en toda América Latina.
La Red ya cuenta con más de 60 funcionarios públicos comprometidos con la transparencia y la co-creación de políticas públicas con su comunidad en las alcaldías de México, Ecuador, El Salvador y Guatemala. Ahora queremos ampliar nuestra red e invitamos a todos los municipios de la región a ser parte de esta iniciativa.

Nuestra coordinadora de proyecto, Vanessa González, explicó que “La desconfianza y lejanía que existe entre nuestros gobiernos y los habitantes de América Latina llegó a un punto límite. Algunas de las causas son los escándalos de corrupción y la falta de participación de la ciudadanía en la toma de decisiones. Desde Ciudadanía Inteligente, estamos convencidos de que podemos tener municipios más abiertos, más innovadores, más inclusivos y participativos y por eso esperamos que este proyecto invite a gobiernos locales a comprometerse con su comunidad.”

Las inscripciones ya se encuentran disponibles en la web [https://abrealcaldias.org/](https://abrealcaldias.org/) y pueden aplicar funcionarios públicos locales que trabajen directamente en su municipio, así como  regidores municipales, concejales, entre otros. La iniciativa también está abierta a personas de la sociedad civil, academia u organizaciones no gubernamentales que colaboran actualmente en procesos de gestión pública local.
