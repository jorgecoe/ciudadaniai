---
layout: post
title: 'Political Violence is a Permanent Attack on Democracies'
intro: 'We demand protection measures from all Latin American States.'
day: 23
month: June
date: 2020-03-21 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'Political Violence is a Permanent Attack on Democracies'
img: violence.png
caption:
---
Latin America’s origins are marked by expropriation and violence towards indigenous, original, African and African-descendant people, who through colonization were enslaved and abandoned by the State since its constitution. Even though today most countries consider themselves “independent,” the social and racial effects of colonialism are not over. In fact, exclusion, inequality, and violence are still rooted intrinsically in the social structure of Latin American countries.

This means that the social, educational, and political processes installed in the region maintain the essence and part of the heritage of this colonialism, which had as its norm imbalances and asymmetries in access to power, representation, and social rights.

Despite this, and due to the tireless struggles of the grassroots social movements and the determination of some leaders, it has been possible to advance in the representation and repositioning of historically underrepresented and violated groups. However, participation in the spheres of power has been brief and has not had the real capacity to permanently shift the control structures established by colonialism.

The economic and political elites of Latin America and the Caribbean have managed to carry out a process of moral and political intervention that appears to promote and expand social rights. It has been carried out through a very "small" State to defend and guarantee these rights but, at the same time, a "large" State, for those processes related to convictions, penalties, and death. These processes lead us today to the materialization of the termed ['necropolitics'](https://elpais.com/internacional/2019/07/09/america/1562688743_395031.html), a process where the states promote policies that cause death in their citizens.

Political and social violence against women, as well as in other groups already mentioned, occurs precisely in states of invisibility and non-protection, becoming the center of a neo-colonial advance in Latin American politics. In fact, from March to June 2020, in the context of the COVID-19 crisis, violence against women increased by 22% in Brazil, according to data from the country's Public Security Forum. Besides, police violence against slum residents and black population has doubled. These are also significant fatalities of COVID-19.

Countries such as Chile, Argentina, Mexico, and Ecuador have seen domestic violence and violence against women increase considerably in times of quarantine. Mexico and Bolivia have advanced legislation on gender quotas for better representation in spaces of power. However, women citizens face daily crimes and harassment aimed at curtailing the political participation of women, in many cases reaching physical violence, and even murder.

Young people from impoverished backgrounds, grassroot leaders, and political activists are victims of criminalization by the State. That is, they usually live under precarious working conditions, increasing the curve of insecurity, attacks, and limitations to obtain a space in decision-making processes (heightened in times of pandemic).

In this sense, the practices understood as political violence materialize in different areas: physical, economic, psychological, and even virtual, where they have found a safe space for dissemination.

For this reason, it is essential to promote actions that allow the political participation of these vulnerable groups. Today more than ever, attention must be paid to material conditions, but also social ties, both being part of the vital infrastructure for safe political participation that does not put citizens at risk.

We must confront and destroy the normalization of political violence. In this way, we can defend democracy and promote the strengthening of public institutions.
We demand protection measures from all Latin American States!

* National bills defending supranational frameworks on political violence.

* More support in initiatives and organizations that protect minority groups and their political leaders.

* Guarantee the spaces of representation of groups historically violated in the construction of public policies.

* The reconfiguration of state force, with a particular focus on reducing overreaction.

* Creation of campaigns at a national level that shows inequalities of gender, race, class, sexuality, and territory.


Here you can check the [video](https://www.youtube.com/watch?v=-u8VT-yL8NQ)
