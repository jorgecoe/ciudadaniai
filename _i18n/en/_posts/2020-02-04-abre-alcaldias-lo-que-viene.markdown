---
layout: post
title: 'This is how our School of Municipal Innovation goes'
intro: 'We strive to incorporate the citizen voice in public policies.'
day: 04
month: Feb
date: 2020-01-27 12:00:00 -0300)
categories: Actualidad
highlighted: false
share_message: 'This is how our School of Municipal Innovation goes.'
img: Frame 5.jpg
caption:
---
At Smart Citizenship Foundation, we started the year 2020 with full force to boost our School of Municipal Innovation project, that seeks to train Latin American cities to innovate in the public sphere with citizenship participation. We started with a call to civil society organizations interested in supporting our initiative. Today we have made great progress with the signature of alliances with four organizations committed to innovation and democracy in each of the countries in which we will work for the next two years. These are [Mexiro](http://www.mexiro.org/), in Mexico; [Non-fiction](https://www.no-ficcion.com/), in Guatemala; **ACTION** in El Salvador, and [the Observatory of Public Policies of Guayaquil](https://oppguayaquil.org/) in Ecuador. For the Smart Citizenship team it is a great honor to be able to work together with highly trained organizations committed to social change in their countries and in the region.

Our next step will be the selection of the municipalities that will be part of the project. How does it going to work? In each country we will select 10 cities that will attend our School of Municipal Innovation, and afterwards they will implement pilot projects for the co-creation of public policies, where local governments effectively incorporate citizenship into their design and implementation of innovation processes. For this selection we have defined the following steps:

**Step one**: mapping cities in the region committed to democratic practices of open government, public innovation, citizenship participation and technology for public interest.

**Step two**: launch an open call for municipalities to apply by presenting proposals indicating the working teams and public policy projects that they intend to develop in a participatory manner.

**Step three**: Invitation to mayors of strategic cities in the region to apply for the program indicating a trained municipal team.

**Step four**: selection of the best candidacies, reaching up to 10 selected cities in each country, communicating the results publicly.

With this plan, we hope that by March we already have the municipal teams selected and ready to start our school. This will be a great opportunity for our region (and even for the world), with an unprecedented school that will provide modern and simple resources for governments opening and to get citizenship closer to their local administrations. We believe that cities are strategic political instances and important decision centers that directly affect citizens lives. With the School of Municipal Innovation we aim to regain the confidence of the inhabitants of Latin America in their local governments and to build a more just, inclusive and sustainable democratic future for all.
