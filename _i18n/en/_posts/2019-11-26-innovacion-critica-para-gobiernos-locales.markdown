---
layout: post
title: 'Critical innovation for local governments'
intro: A few days ago, we shared out citizen participation tools at Smart City Expo
day: 27
month: Nov
date: 2019-11-27 12:00:00 -0300)
share_message: 'Critical innovation for local governments'
img: 27-11-19.jpg
caption:
categories: Actualidad
highlighted: false
---

***Part of our team traveled to [Smart City Expo](http://www.smartcityexpo.com/) to share tools to include citizens in decision making processes at the municipal level.***

The workshop was organized within the framework of the [Digital Future Society Summit](https://digitalfuturesociety.com/agenda/digital-future-society-summit-at-scewc-2019/), an event organized by the think tank of the same name, which we are also part, and that took place within the framework of the [Smart City Expo World Congress](http://www.smartcityexpo.com/en/home), a huge and massive event that brings together thousands of people, organizations and companies around cities, technology, future and sustainability.


Our session was intense: we had assistants from all over the world (Haiti, France, Poland, Spain, Chile, among other countries) who did a practical exercise on how to co-create solutions to public problems between governments and citizens. To do this, we put at their disposal some of our tools such as LabCívico, the Abre methodology, and the critical view on the use of the technology proposed by the [A + Alliance](aplusalliance.org).

In relation to that, we shared some relevant data to make those who work with governments aware that technology is not neutral, and although it can strengthen democracies, if it is not audited and transparent, it can be extremely harmful. Some examples:

Women are 47% more likely to be seriously injured in a traffic accident and 17% more likely to die in one of them, because car seats are not planned for people of lower height, who need to be closer to pedals. In addition, tests with female crash are most often done in the passenger seat, ignoring that women drive.
Facial recognition systems, which are used for sensitive actions as criminal identification, are much more accurate in identifying white people, and have problems identifying people from communities that are already discriminated against and criminalized, such as african-descendants.


We have emphasized that technology is a political phenomenon, and that we cannot speak of public innovation without considering the effective citizen participation in the innovation processes. With that, we intended to advance our institutional agenda on technology of public interest, where citizens are an effective part of the innovation process, from its conception to its delivery, where social problems are prioritized in the creation of new technologies, and the technological development of cities respect citizens digital rights.

In summary, we made available to international leaders an overview of our ten years of experience promoting citizens participation and the responsible and strategic use of technology, principles that we hope to expand and incorporate in the cities agenda in order to build a more democratic and inclusive future.
