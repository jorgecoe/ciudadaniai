---
ref: pau
lang: en
sede: chile
name: Paulina Vasconcelo
function: Designer
bio: Designer from the P. Catholic University of Chile. Love of information design, feminism and cats.
image: pau.jpg
email: pvasconcelo@ciudadaniai.org
network_twitter:
network_linkedin:
network_github: 
network_googleplus:
network_facebook:
network_instagram:
active: true
---
