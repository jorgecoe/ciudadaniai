---
ref: vero
lang: en
sede: chile
name: Verónica Maldonado
function: Coordinator of Administration and Finances
bio: Public Administrator with a mention in Public Management from the University of Chile. She has dedicated herself to the administration and finance area of different companies and organizations in the cultural sector.
image: vero.jpg
email: vmaldonado@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
