---
ref: fede
lang: pt
sede: chile
name: Federica Sánchez
function: Coordenadora de Advocacy e Conteúdo
bio: Graduado em Relações Internacionais. Com mestrado em Ciência Política pela University of Houston, Estados Unidos. Doutor em Ciência Política pela Universidade Católica do Chile.
image: Federica.png
email: fsanchez@ciudadaniai.org
network_twitter: https://twitter.com/fedestaniak
network_linkedin: https://www.linkedin.com/in/federica-sanchez-staniak-860042b9/?locale=es_ES
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
