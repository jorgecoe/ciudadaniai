---
ref: Miguel
lang: pt
sede: chile
name: Miguel
function: Community Manager
bio: Especialista em redes sociais e ativista em defesa da democracia. Amante de longas conversas e abraços.
image: miguel.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
