---
ref: octavio
lang: es
sede: chile
name: Octavio Del Favero
function: Director Ejecutivo
bio: Abogado de la Universidad de Chile con máster en Política Comparada de la London School Economics.
image: octavio.jpg
email: odfavero@ciudadaniai.org
network_twitter:  https://twitter.com/octaviodfb
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
sede: chile
---
