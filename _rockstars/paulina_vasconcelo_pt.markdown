---
ref: pau
lang: pt
sede: chile
name: Paulina Vasconcelo
function: Designer
bio: Designer pela Universidade Católica do Chile. Com uma paixão por design de informação, feminismo e gatos.
image: pau.jpg
email: pvasconcelo@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
