---
ref: bruna
lang: pt
sede: chile
name: Bruna
function: Human Resources Assistant
bio: Fiel assistente administrativo de 4 patas. Amante da boa comida e das carícias da equipe.
image: Bruna.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
