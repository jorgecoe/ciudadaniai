---
ref: fede
lang: en
sede: chile
name: Federica Sánchez
function: Advocacy and Content Coordinator
bio: Degree in International Relations. Holds a master's in Political Science from the University of Houston, United States. Doctor in Political Science from the Catholic University of Chile.
image: Federica.png
email: fsanchez@ciudadaniai.org
network_twitter: https://twitter.com/fedestaniak
network_linkedin: https://www.linkedin.com/in/federica-sanchez-staniak-860042b9/?locale=es_ES
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
