---
ref: bruna
lang: es
sede: chile
name: Bruna
function: Asistente de Recursos Humanos
bio: Fiel asistente administrativa de 4 patas. Amante de la buena mesa y las caricias del equipo.
image: Bruna.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
