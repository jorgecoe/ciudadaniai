---
ref: Miguel
lang: es
sede: chile
name: Miguel
function: Community Manager
bio: Experto en redes sociales y activista en defensa de la democracia. Amante de conversaciones largas y los regaloneos.
image: miguel.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
