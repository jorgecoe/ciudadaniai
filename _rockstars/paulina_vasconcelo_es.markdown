---
ref: pau
lang: es
sede: chile
name: Paulina Vasconcelo
function: Diseñadora
bio: Diseñadora de la Universidad Católica de Chile. Su pasión es el diseño de información, el feminismo y los gatos.
image: pau.jpg
email: pvasconcelo@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
