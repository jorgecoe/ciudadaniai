---
ref: apolo
lang: pt
sede: chile
name: Apolo
function: Apoyo en Facilitación
bio: Facilitador virtual, especialista em construção colaborativa, quebra-gelos e harmonizando o fortalecimento das democracias em tempos de confinamento.
image: apolo.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
