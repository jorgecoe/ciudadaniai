---
ref: malu
lang: pt
name: Maria Luiza Freire
function: Coordenadora de Projetos e Pesquisas
bio: Advogada pela PUC-Rio, feminista e descolonialista. Mestranda em políticas públicas e planejamento urbano na América Latina pelo IPPUR / UFRJ. Inovando em participação política.
image: malu.jpg
email: mfreire@cidadaniainteligente.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
