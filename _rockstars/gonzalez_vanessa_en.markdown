---
ref: vanessa
lang: en
sede: chile
name: Vanessa González
function: Project and Research Coordinator
bio: Political Scientist from Universidad Católica (Chile). She holds a degree in Social Sciences at the same university with a major  in International Relations, and two minors in Public Politics and Health Promotion and Care.
image: vane.jpg
email: vanessa@ciudadaniai.org
network_linkedin: https://www.linkedin.com/in/vanessa-gonz%C3%A1lez-ratsch-4824a7143/
active: true
---
