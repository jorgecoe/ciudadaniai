---
ref: felipe
lang: en
sede: chile
name: Felipe Perry
function: Developer
bio: Developer derived journalist. Linuxero by vocation and convinced that technology can help us live better.
image: felipe.png
email: fperry@ciudadaniai.org
network_twitter: https://twitter.com/feliperry 
network_linkedin:
network_github: https://github.com/pedregalux
network_googleplus:
network_facebook:
network_instagram:
active: true
---
