---
ref: abel
lang: en
sede: chile
name: Abel Escobar
function: Front-End Designer
bio: Graphic designer from Duoc UC, specialized in Front-end Design and Development.
image: abel.jpg
email: aescobar@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
