---
ref: abel
lang: es
sede: chile
name: Abel Escobar
function: Diseñador Front-End
bio: Diseñador Gráfico de Duoc UC, especializado en Diseño y Desarrollo Front-end.
image: abel.jpg
email: aescobar@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
