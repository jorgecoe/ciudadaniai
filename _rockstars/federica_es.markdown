---
ref: fede
lang: es
sede: chile
name: Federica Sánchez
function: Coordinadora de Incidencia y Contenidos
bio: Licenciada en Relaciones Internacionales. Con máster en Ciencia Política de la University of Houston, Estados Unidos. Doctora en Ciencia Política de la Universidad Católica de Chile.
image: Federica.png
email: fsanchez@ciudadaniai.org
network_twitter: https://twitter.com/fedestaniak
network_linkedin: https://www.linkedin.com/in/federica-sanchez-staniak-860042b9/?locale=es_ES
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
