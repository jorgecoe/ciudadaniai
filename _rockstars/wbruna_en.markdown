---
ref: bruna
lang: en
sede: chile
name: Bruna
function: Human Resources Assistant
bio: Faithful 4 legs administrative assistant. Lover of good food and the caresses of the team.
image: Bruna.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
