---
ref: apolo
lang: en
sede: chile
name: Apolo
function: Facilitation support
bio: Virtual facilitator, expert in collaborative construction, icebreaker and enhances the strengthening of democracies in times of confinement.
image: apolo.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
