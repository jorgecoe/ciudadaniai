---
ref: Miguel
lang: en
sede: chile
name: Miguel
function: Community Manager
bio: Expert in social networks and activist in defense of democracy. Lover of long conversations and cuddles.
image: miguel.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
