---
ref: abel
lang: pt
sede: chile
name: Abel Escobar
function: Designer Front-End
bio: Designer Gráfico pela Duoc UC, com especialização em Design e Desenvolvimento de Front-end.
image: abel.jpg
email: aescobar@ciudadaniai.org
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
