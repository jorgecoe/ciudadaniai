---
ref: apolo
lang: es
sede: chile
name: Apolo
function: Apoyo en Facilitación
bio: Facilitador virtual, experto en construcción colaborativa, rompehielos y amenizar el fortalecimiento de las democracias en épocas de confinamiento.
image: apolo.jpg
email:
network_twitter:
network_linkedin:
network_github:
network_googleplus:
network_facebook:
network_instagram:
active: true
---
