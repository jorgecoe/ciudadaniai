---
lang: pt
name: "2014"

title: "Celebramos nosso quinto ano com uma transição de liderança e refinamento do enfoque estratégico. "
description: "Desenvolvemos ferramentas para vigilância das eleições, controle legislativo, a comparação das propostas de lei, a liberdade de acesso a informação, o conflito de interesses, o seguimento das promessas de governo e o financiamento de campanhas. A maioria destas ferramentas foram desenvolvidas inicialmente para o sistema chileno, mas isso mudou. Este ano eles foram usados ​​em mais de 10 casos e não apenas nos países da América Latina, mas também em países tão distantes como Marrocos."
---
