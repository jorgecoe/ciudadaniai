---
lang: es
name: "2011"
title: "Recopilamos información del mundo público para transferirlo de manera amigable y simple a la ciudadanía."
description: "Abrimos el espacio con nuestros proyectos para saber en porcentajes cuánto cumplen nuestras autoridades y cómo legisla cada parlamentario o parlamentaria en Chile."

---
