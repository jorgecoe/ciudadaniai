---
layout: proposal
country: Chile
lang: es
title: "Brasil / 2018"
summary: "Se registró un aumento del 59% en tiroteos. más de 4 mil homicidios
aumentó en un 40% la muertes debido a la intervención policial.
"
date: 2019-10-21 12:00:00 -0300
---

***[10 meses de intervención en las calles de Río de Janeiro.](http://www.observatoriodaintervencao.com.br/dados/relatorios1/) Se registró un aumento del 59% en tiroteos Más de 4 mil homicidios Aumentó en un 40% la muertes debido a la intervención policial.***
